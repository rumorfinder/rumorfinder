﻿using System;

namespace RumorFinder.Foundation.Util
{

    internal class ArticleParserApiException : Exception
    {

        #region Static Fields and Constants

        private const string FailedRequestMessage = "The request has failed";

        #endregion

        #region Properties

        public static ArticleParserApiException FailedRequestException =>
            new ArticleParserApiException(ArticleParserApiException.FailedRequestMessage);

        #endregion

        #region Constructors

        public ArticleParserApiException()
        { }

        public ArticleParserApiException(string message) : base(message)
        { }

        public ArticleParserApiException(string message, Exception inner) : base(message, inner)
        { }

        #endregion

    }

}