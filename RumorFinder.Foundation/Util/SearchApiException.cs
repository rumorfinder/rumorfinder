﻿using System;

namespace RumorFinder.Foundation.Util
{

    /// <inheritdoc />
    /// <summary>
    ///     Represents an exception that the search api throws when it encounters a problem
    /// </summary>
    internal class SearchApiException : Exception
    {

        #region Static Fields and Constants

        private const string TimeoutMessage = "The request has timedout";
        private const string FailedRequestMessage = "The request has failed";

        #endregion

        #region Properties

        public static SearchApiException TimeoutException => new SearchApiException(SearchApiException.TimeoutMessage);

        public static SearchApiException FailedRequestException =>
            new SearchApiException(SearchApiException.FailedRequestMessage);

        #endregion

        #region Constructors

        public SearchApiException()
        { }

        public SearchApiException(string message) : base(message)
        { }

        public SearchApiException(string message, Exception inner) : base(message, inner)
        { }

        #endregion

    }

}