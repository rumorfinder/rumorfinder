﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using RumorFinder.NativeHttp;

namespace RumorFinder.Foundation.Util
{

    internal class NativeHttpProgressClient : HttpProgressClient
    {

        #region Fields

        private readonly NativeMessageHandler _messageHandler;

        #endregion

        #region Constructors

        public NativeHttpProgressClient() : this(new NativeMessageHandler())
        { }

        public NativeHttpProgressClient(NativeMessageHandler handler) : base(handler) =>
            _messageHandler = handler ?? throw new ArgumentNullException(nameof(handler));

        #endregion

        /// <summary>
        ///     sends an HTTP request as an asynchronous operation
        /// </summary>
        /// <param name="request">the request to send</param>
        /// <param name="cancellationToken">the cancellation token to cancel the operation</param>
        /// <param name="progress">the object used to report the progress of the operation</param>
        /// <returns>the HTTP response</returns>
        public override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            // register for progress reporting and delegate the request
            _messageHandler.RegisterProgressCallback(request, progress);
            return base.SendAsync(request, cancellationToken);
        }

    }

}