﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RumorFinder.Foundation.Util
{

    internal abstract class HttpProgressClient : HttpClient
    {

        #region Constructors

        protected HttpProgressClient()
        { }

        protected HttpProgressClient(HttpMessageHandler handler) : this(handler, true)
        { }

        protected HttpProgressClient(HttpMessageHandler handler, bool disposeHandler) :
            base(handler, disposeHandler)
        { }

        #endregion

        public abstract Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken,
            IProgress<float> progress);

        #region Private Helpers

        private Uri CreateUri(string uri) =>
            string.IsNullOrEmpty(uri) ? null : new Uri(uri, UriKind.RelativeOrAbsolute);

        #endregion

        #region REST Send Overloads

        public Task<HttpResponseMessage> GetAsync(
            string requestUri,
            CancellationToken cancellationToken,
            IProgress<float> progress) => GetAsync(CreateUri(requestUri), cancellationToken, progress);

        public Task<HttpResponseMessage> GetAsync(
            Uri requestUri,
            CancellationToken cancellationToken,
            IProgress<float> progress) => SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri),
                                                    cancellationToken,
                                                    progress);

        public Task<HttpResponseMessage> PostAsync(
            string requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress) => PostAsync(CreateUri(requestUri), content, cancellationToken, progress);

        public Task<HttpResponseMessage> PostAsync(
            Uri requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = content
            };
            return SendAsync(request, cancellationToken, progress);
        }

        public Task<HttpResponseMessage> PutAsync(
            string requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress) => PutAsync(CreateUri(requestUri), content, cancellationToken, progress);

        public Task<HttpResponseMessage> PutAsync(
            Uri requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, requestUri)
            {
                Content = content
            };
            return SendAsync(request, cancellationToken, progress);
        }

        public Task<HttpResponseMessage> PatchAsync(
            string requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress) => PatchAsync(CreateUri(requestUri), content, cancellationToken, progress);

        public Task<HttpResponseMessage> PatchAsync(
            Uri requestUri,
            HttpContent content,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            var request = new HttpRequestMessage(new HttpMethod("PATCH"), requestUri)
            {
                Content = content
            };
            return SendAsync(request, cancellationToken, progress);
        }

        public Task<HttpResponseMessage> DeleteAsync(
            string requestUri,
            CancellationToken cancellationToken,
            IProgress<float> progress) => DeleteAsync(CreateUri(requestUri), cancellationToken, progress);

        public Task<HttpResponseMessage> DeleteAsync(
            Uri requestUri,
            CancellationToken cancellationToken,
            IProgress<float> progress) => SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri),
                                                    cancellationToken,
                                                    progress);

        #endregion

    }

}