﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace RumorFinder.Foundation.Util.Event_Arguments
{

    /// <summary>
    ///     Represents a collection changed event args for the Reset event action
    /// </summary>
    public class CollectionChangedResetArgs : NotifyCollectionChangedEventArgs
    {

        #region Properties

        public IEnumerable<int> DeletedIndexes { get; }

        #endregion

        #region Constructors

        public CollectionChangedResetArgs(IEnumerable<int> deletedIndexes) : base(NotifyCollectionChangedAction.Reset) =>
            DeletedIndexes = deletedIndexes;

        #endregion

    }

}