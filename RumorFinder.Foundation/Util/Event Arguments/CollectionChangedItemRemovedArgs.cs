﻿using System.Collections;
using System.Collections.Specialized;

namespace RumorFinder.Foundation.Util.Event_Arguments
{

    /// <summary>
    ///     Represents a collection changed event args for the Remove event action
    /// </summary>
    public class CollectionChangedItemRemovedArgs : NotifyCollectionChangedEventArgs
    {

        #region Properties

        public int RemovedItemIndex { get; }

        #endregion

        #region Constructors

        public CollectionChangedItemRemovedArgs(object removedObject, int removedItemIndex) :
            base(NotifyCollectionChangedAction.Remove, new ArrayList {removedObject}, removedItemIndex) =>
            RemovedItemIndex = removedItemIndex;

        #endregion

    }

}