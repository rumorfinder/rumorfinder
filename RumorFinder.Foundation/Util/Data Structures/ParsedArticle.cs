﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace RumorFinder.Foundation.Util.Data_Structures
{

    /// <summary>
    ///     represents a Parsed online article
    /// </summary>
    internal readonly struct ParsedArticle
    {

        public IEnumerable<string> Paragraphs { get; }

        /// <summary>
        ///     creates a new parsed article
        /// </summary>
        /// <param name="paragraphs">The parsed text of the article in the form of paragraphs</param>
        public ParsedArticle(IEnumerable<string> paragraphs)
        {
            Paragraphs = ImmutableList.CreateRange(paragraphs);
        }

    }

}