﻿using System;
using Newtonsoft.Json;

namespace RumorFinder.Foundation.Util
{

    [JsonObject(MemberSerialization.OptIn)]
    internal readonly struct TimedCertificate
    {

        #region Static Fields and Constants

        public static readonly TimedCertificate None = default;

        #endregion

        #region Properties

        [JsonProperty]
        private DateTime ExpirationDate { get; }

        public bool IsValid => ExpirationDate > DateTime.Now;

        #endregion

        #region Constructors

        [JsonConstructor]
        public TimedCertificate(DateTime expirationDate) => ExpirationDate = expirationDate;

        #endregion

        /// <summary>
        ///     renews the certificate for the additional time supplied
        /// </summary>
        /// <param name="addition">the timespan to extend the expiration date by</param>
        /// <returns>a renewed certificate</returns>
        public TimedCertificate Renew(TimeSpan addition) => new TimedCertificate(DateTime.Now.Add(addition));

    }

}