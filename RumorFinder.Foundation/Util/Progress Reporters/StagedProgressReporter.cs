﻿using System;

namespace RumorFinder.Foundation.Util.Progress_Reporters
{

    /// <summary>
    ///     represents a multi step report of progress in the form of percentage in the range of 0 - 100
    /// </summary>
    internal class StagedProgressReporter : IProgress<float>
    {

        #region Fields

        private readonly IProgress<float> _reporter;

        private float _currentStageProgress;

        #endregion

        #region Properties

        public int CompletedStages { get; set; }
        public int StagesCount { get; }

        #endregion

        #region Constructors

        public StagedProgressReporter(IProgress<float> reporter, int stagesCount) =>
            (_reporter, StagesCount) = (reporter, stagesCount);

        #endregion

        /// <inheritdoc />
        /// <summary>Reports a progress update.</summary>
        /// <remarks>only accepts values between 0 to 100.</remarks>
        /// <param name="value">The value of the updated progress.</param>
        public void Report(float value)
        {
            // check the bounds of the value
            if (value > 1)
                value = 1;
            else if (value < 0)
                value = 0;

            _currentStageProgress = value;
            float currentOverallProgress = 1f * CompletedStages / StagesCount;
            currentOverallProgress += _currentStageProgress / StagesCount;
            _reporter?.Report(currentOverallProgress);
        }

        /// <summary>
        ///     completes the current stage, if the stage hasn't already finished it will be reported as completed
        /// </summary>
        public void CompleteCurrentStage()
        {
            if (CompletedStages == StagesCount)
                return;
            if (_currentStageProgress != 1)
                Report(1);
            CompletedStages++;
        }

    }

}