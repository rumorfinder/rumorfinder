﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using OpenTextSummarizer;

namespace RumorFinder.Foundation
{

    /// <summary>
    ///     represents a type encapsulating rumor information
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Rumor
    {

        #region Static Fields and Constants

        private const string NonMatchingCategoriesExceptionMessage =
            "Cannot update from a other of a different category";

        //no idea why the initial separator can't be \n because it screws up the summarizer
        private const string InitialParagraphSeparator = "\n";
        private const string ParagraphSeparator = "\n";
        private const string UnitSeparator = "\n\n";

        #endregion

        #region Fields

        [JsonProperty("contentUnits")]
        private readonly ImmutableList<string> _contentUnits;

        [JsonProperty("updatesUnits")]
        private readonly ImmutableList<string> _updatesUnits;

        #endregion

        #region Properties

        [JsonProperty("category")]
        public RumorCategory Category { get; }

        public string Content => string.Join(Rumor.UnitSeparator, _contentUnits);

        public string Updates => string.Join(Rumor.UnitSeparator, _updatesUnits);

        public int UpdatesCount => _updatesUnits.Count;

        public bool HasUpdates => UpdatesCount > 0;

        public bool IsEmpty => _contentUnits.IsEmpty && _updatesUnits.IsEmpty;

        #endregion

        #region Constructors

        /// <summary>
        ///     private wrapper constructor for inside use
        /// </summary>
        /// <param name="category"></param>
        /// <param name="contentUnits"></param>
        /// <param name="updatesUnits"></param>
        [JsonConstructor]
        private Rumor(
            RumorCategory category,
            ImmutableList<string> contentUnits,
            ImmutableList<string> updatesUnits) =>
            (Category, _contentUnits, _updatesUnits) = (category, contentUnits, updatesUnits);

        /// <summary>
        ///     initializes a new Rumor Content
        /// </summary>
        /// <param name="category">the category of the other</param>
        /// <param name="initialContentParagraphs">the initial paragraphs for the other</param>
        internal Rumor(RumorCategory category, IEnumerable<string> initialContentParagraphs) :
            this(category, string.Join(Rumor.InitialParagraphSeparator, initialContentParagraphs))
        { }

        /// <summary>
        ///     initializes a new Rumor Content
        /// </summary>
        /// <param name="category">the category of the rumor</param>
        /// <param name="initialContent">the initial content, empty strings are ignored</param>
        internal Rumor(RumorCategory category, string initialContent)
        {
            Category = category;
            _contentUnits = ImmutableList<string>.Empty;
            if (!string.IsNullOrEmpty(initialContent))
                _contentUnits = _contentUnits.Add(initialContent);
            _updatesUnits = ImmutableList<string>.Empty;
        }

        #endregion

        /// <summary>
        ///     combines the two list so that only the items from list2 that are not in list 1 will be added
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns>a combined list with no duplicates</returns>
        private static ImmutableList<string> Union(ImmutableList<string> list1, ImmutableList<string> list2)
        {
            var uniqueItems = from s in list2
                              where !list1.Contains(s)
                              select s;
            return list1.AddRange(uniqueItems);
        }

        /// <summary>
        ///     add an update to the current Rumor
        /// </summary>
        /// <param name="updateSource">the other to find updates from</param>
        /// <returns>the foundUpdates Rumor object</returns>
        /// <param name="foundUpdates">Will hold the value of whether new information was found.</param>
        /// <exception cref="InvalidOperationException">
        ///     if the update source's category does not match this Rumor's category, an
        ///     exception will be raised
        /// </exception>
        internal Rumor UpdateFrom(Rumor updateSource, out bool foundUpdates)
        {
            if (Category != updateSource.Category)
                throw new InvalidOperationException(Rumor.NonMatchingCategoriesExceptionMessage);

            // get all the string units from the update source
            var units = updateSource._contentUnits.Union(updateSource._updatesUnits);
            // extract all the paragraphs from the update source
            IEnumerable<string> paragraphs = from unit in units
                                             from paragraph in Regex.Split(unit, Rumor.ParagraphSeparator)
                                             select paragraph;

            var updateBuilder = new StringBuilder();
            bool builderEmpty = true;
            foreach (string paragraph in paragraphs)
            {
                if (UnitsContain(paragraph)) continue;

                // make sure to separate the paragraphs
                if (builderEmpty)
                    builderEmpty = false;
                else
                    updateBuilder.Append(Rumor.ParagraphSeparator);
                updateBuilder.Append(paragraph);
            }

            // if the builder isn't empty we have found updates
            foundUpdates = !builderEmpty;

            // if no updates were found return the current object
            if (builderEmpty)
                return this;

            // else add the update
            return Wrap(Category, _contentUnits, _updatesUnits.Add(updateBuilder.ToString()));

            // check if the paragraph is already present in the current units
            bool UnitsContain(string paragraph) => Content.Contains(paragraph) || Updates.Contains(paragraph);
        }

        /// <summary>
        ///     merge the updates with the Content of this Rumor
        /// </summary>
        /// <returns>the Rumor object with the merged updates</returns>
        internal Rumor MergeUpdates() =>
            Wrap(Category, Union(_contentUnits, _updatesUnits), ImmutableList<string>.Empty);

        /// <summary>
        ///     summarizes the content and updates of the Rumor
        /// </summary>
        /// <param name="args">the summarizer arguments to consider when summarizing</param>
        /// <returns>a summarized version of the current Rumor</returns>
        internal Rumor Summarize(SummarizerArguments args)
        {
            return Wrap(Category, SummarizeUnits(_contentUnits), SummarizeUnits(_updatesUnits));

            //------------Helper Method-----------------
            //summarize a list of units by summarizing them separately
            ImmutableList<string> SummarizeUnits(IEnumerable<string> units)
            {
                var summarizedUnits = new List<string>();
                foreach (string unit in units)
                {
                    IEnumerable<string> summarizedParagraphs;
                    string[] preSummarizedParagraphs = unit.Split(Convert.ToChar(Rumor.ParagraphSeparator));

                    // doing this to avoid unnecessary summarization 
                    if (preSummarizedParagraphs.Length <= args.MaxSummarySentences)
                    {
                        summarizedParagraphs = preSummarizedParagraphs;
                    }
                    else
                    {
                        summarizedParagraphs = Summarizer.Summarize(new DirectTextContentProvider(unit),
                                                                    args).Sentences;
                    }

                    summarizedUnits.Add(string.Join(Rumor.ParagraphSeparator, summarizedParagraphs));
                }

                return ImmutableList<string>.Empty.AddRange(summarizedUnits);
            }
        }

        #region Private Helpers

        /// <summary>
        ///     private helper to wrap new values into a new Rumor Object without wasting memory
        /// </summary>
        /// <param name="category"></param>
        /// <param name="contentUnits"></param>
        /// <param name="updateUnits"></param>
        /// <returns></returns>
        private Rumor Wrap(
            RumorCategory category,
            ImmutableList<string> contentUnits,
            ImmutableList<string> updateUnits)
        {
            if ((Category, _contentUnits, _updatesUnits) == (category, contentUnits, updateUnits))
                return this;
            return new Rumor(category, contentUnits, updateUnits);
        }

        #endregion

    }

}