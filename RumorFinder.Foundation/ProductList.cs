﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RumorFinder.Foundation.Util.Event_Arguments;
using RumorFinder.Foundation.Util.Progress_Reporters;

namespace RumorFinder.Foundation
{

    /// <summary>
    ///     Represents a thread-safe sorted list of products.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ProductList : ICollection<Product>, INotifyCollectionChanged
    {

        #region Fields

        [JsonProperty("internalList")]
        private readonly List<Product> _internalList;

        // we use this lock to ensure the order of notifications sent is 
        // the same order in which they occur
        private readonly object _notificationsLock = new object();

        #endregion

        #region Properties

        /// <summary>
        ///     returns the product at teh given index.
        /// </summary>
        /// <param name="index">The index to lookup for in the list</param>
        /// <returns>The product at the given list.</returns>
        public Product this[int index]
        {
            get
            {
                lock (SyncRoot)
                {
                    return _internalList[index];
                }
            }
        }

        public IComparer<Product> Comparer { get; }

        private object SyncRoot => ((ICollection)_internalList).SyncRoot;

        #endregion

        #region Constructors

        public ProductList() : this(Comparer<Product>.Default)
        { }

        public ProductList(IComparer<Product> comparer)
        {
            _internalList = new List<Product>();
            Comparer = comparer;
        }

        public ProductList(IEnumerable<Product> products) : this(products, Comparer<Product>.Default)
        { }

        public ProductList(IEnumerable<Product> products, IComparer<Product> comparer)
        {
            _internalList = new List<Product>(products);
            _internalList.Sort(comparer);

            foreach (Product product in _internalList)
            {
                product.RumorsChanged -= OnItemChanged;
                product.RumorsChanged += OnItemChanged;
            }

            Comparer = comparer;
        }

        #endregion

        /// <summary>Occurs when the collection changes.</summary>
        /// <remarks>DO NOT attempt to modify the list from an event handler for this event.</remarks>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        ///     Attempts to find updates on every product in this list.
        /// </summary>
        /// <param name="isUserInitiated">Was the operation initiated by the user.</param>
        /// <param name="cancellationToken">The token used to cancel the operation.</param>
        /// <param name="progress">The object used to report back the progress of the operation. Values are given in the range 0-1.</param>
        /// <returns></returns>
        public async Task<OperationStatus> UpdateAllProductsAsync(
            bool isUserInitiated = true,
            CancellationToken cancellationToken = default,
            IProgress<float> progress = null)
        {
            // assign the update tasks
            Task<OperationStatus>[] updateTasks;
            var multiProgressReporter = new MultiSourceProgressReporter(progress, Count);
            lock (SyncRoot)
            {
                updateTasks = (from product in _internalList
                               let p = new Progress<float>()
                               where multiProgressReporter.TryAddSource(p)
                               select product.UpdateRumorsAsync(isUserInitiated, 
                                                                cancellationToken, 
                                                                p)).ToArray();
            }

            // await all the tasks
            await Task.WhenAll(updateTasks).ConfigureAwait(false);

            // asses the completed tasks
            int unsuccessfulCount = 0, failedCount = 0, itemCount = updateTasks.Length;
            foreach (Task<OperationStatus> updateTask in updateTasks)
            {
                // this will complete synchronously because the tasks have already completed 
                switch (await updateTask)
                {
                    case OperationStatus.Failed:
                        failedCount++;
                        break;
                    case OperationStatus.Unsuccessful:
                        unsuccessfulCount++;
                        break;
                }
            }

            // select the appropriate status for the operation according to the results
            OperationStatus status = OperationStatus.Successful;
            if (unsuccessfulCount == itemCount)
                status = OperationStatus.Unsuccessful;
            else if (failedCount == itemCount)
                status = OperationStatus.Failed;

            return status;
        }

        /// <summary>
        ///     triggers the collection changed event with the given args
        /// </summary>
        /// <param name="args"></param>
        private void OnCollectionChanged(NotifyCollectionChangedEventArgs args) =>
            CollectionChanged?.Invoke(this, args);

        /// <summary>
        ///     Called when the rumors of an item of the list get changed.
        ///     This method makes sure the list will stay sorted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnItemChanged(object sender, EventArgs e)
        {
            lock (_notificationsLock)
            {
                CollectionChangedItemsMovedArgs args;
                lock (SyncRoot)
                {
                    var preSortCopy = new Product[_internalList.Count];
                    _internalList.CopyTo(preSortCopy);

                    // sort the list
                    _internalList.Sort(Comparer);

                    // create the indexes dictionary that will hold
                    // the XOR'd value of the old and new indexes
                    var indexes = new Dictionary<Product, int>();
                    for (int i = 0; i < _internalList.Count; i++)
                    {
                        Product preSortItem = preSortCopy[i];
                        Product currentItem = _internalList[i];

                        // initialize the dictionary
                        if (!indexes.ContainsKey(preSortItem))
                        {
                            indexes[preSortItem] = -1;
                        }
                        if (!indexes.ContainsKey(currentItem))
                        {
                            indexes[currentItem] = -1;
                        }

                        indexes[preSortItem] ^= i;
                        indexes[currentItem] ^= i;
                    }

                    var movedObjects = new ArrayList();
                    var changes = new List<(int oldIndex, int newIndex)>();

                    // alert any changes to event listeners
                    for (int newIndex = 0; newIndex < _internalList.Count; newIndex++)
                    {
                        Product currentItem = _internalList[newIndex];
                        int index = indexes[currentItem];

                        // if the index is -1 then the product's index was
                        // not changed in the sort process
                        if (index is -1) continue;

                        // extract the old index
                        int oldIndex = index ^ newIndex ^ -1;

                        movedObjects.Add(currentItem);
                        changes.Add((oldIndex, newIndex));
                    }

                    args = new CollectionChangedItemsMovedArgs(movedObjects, changes);
                }

                // notify of the changes
                OnCollectionChanged(args);
            }
        }

        #region Collection Methods

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public IEnumerator<Product> GetEnumerator()
        {
            lock (SyncRoot)
            {
                foreach (Product product in _internalList)
                {
                    yield return product;
                }
            }
        }

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>
        ///     An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the
        ///     collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <remarks>null items are not added to the list</remarks>
        /// <exception cref="T:System.NotSupportedException">
        ///     The <see cref="T:System.Collections.Generic.ICollection`1"></see> is
        ///     read-only.
        /// </exception>
        public void Add(Product item)
        {
            if (item is null)
                return;

            lock (_notificationsLock)
            {
                CollectionChangedItemAddedArgs args;
                lock (SyncRoot)
                {
                    // binary search in order to get the insertion index for the item
                    int index = _internalList.BinarySearch(item, Comparer);

                    // if the item doesn't exist in the 
                    if (index < 0)
                        index = ~index;

                    _internalList.Insert(index, item);

                    // add the event listener
                    item.RumorsChanged += OnItemChanged;

                    args = new CollectionChangedItemAddedArgs(item, index);
                }

                // trigger teh event
                OnCollectionChanged(args);
            }
        }

        /// <summary>Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</summary>
        /// <exception cref="T:System.NotSupportedException">
        ///     The <see cref="T:System.Collections.Generic.ICollection`1"></see> is
        ///     read-only.
        /// </exception>
        public void Clear()
        {
            lock (_notificationsLock)
            {
                CollectionChangedResetArgs args;
                lock (SyncRoot)
                {
                    IEnumerable<int> deletedIndexes = _internalList.Select((product, index) => index);

                    _internalList.Clear();

                    args = new CollectionChangedResetArgs(deletedIndexes);
                }

                // trigger the event
                OnCollectionChanged(args);
            }
        }

        /// <summary>
        ///     Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> contains a specific
        ///     value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <returns>
        ///     true if <paramref name="item">item</paramref> is found in the
        ///     <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
        /// </returns>
        public bool Contains(Product item)
        {
            if (item is null)
                return false;

            lock (SyncRoot)
            {
                // binary search the item and see if it exists in the internal list
                int index = _internalList.BinarySearch(item, Comparer);
                return index >= 0;
            }
        }

        /// <summary>
        ///     Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"></see> to an
        ///     <see cref="T:System.Array"></see>, starting at a particular <see cref="T:System.Array"></see> index.
        /// </summary>
        /// <param name="array">
        ///     The one-dimensional <see cref="T:System.Array"></see> that is the destination of the elements
        ///     copied from <see cref="T:System.Collections.Generic.ICollection`1"></see>. The <see cref="T:System.Array"></see>
        ///     must have zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="array">array</paramref> is null.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     <paramref name="arrayIndex">arrayIndex</paramref> is less than
        ///     0.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        ///     The number of elements in the source
        ///     <see cref="T:System.Collections.Generic.ICollection`1"></see> is greater than the available space from
        ///     <paramref name="arrayIndex">arrayIndex</paramref> to the end of the destination
        ///     <paramref name="array">array</paramref>.
        /// </exception>
        public void CopyTo(Product[] array, int arrayIndex)
        {
            lock (SyncRoot)
            {
                _internalList.CopyTo(array, arrayIndex);
            }
        }

        /// <summary>
        ///     Removes the first occurrence of a specific object from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1"></see>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
        /// <returns>
        ///     true if <paramref name="item">item</paramref> was successfully removed from the
        ///     <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false. This method also returns false if
        ///     <paramref name="item">item</paramref> is not found in the original
        ///     <see cref="T:System.Collections.Generic.ICollection`1"></see>.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        ///     The <see cref="T:System.Collections.Generic.ICollection`1"></see> is
        ///     read-only.
        /// </exception>
        public bool Remove(Product item)
        {
            if (item is null)
                return false;

            lock (_notificationsLock)
            {
                CollectionChangedItemRemovedArgs args;
                lock (SyncRoot)
                {
                    // binary search the item
                    int index = _internalList.BinarySearch(item, Comparer);

                    // if it doesn't exist we return false
                    if (index < 0)
                        return false;

                    _internalList.RemoveAt(index);

                    // make sure to remove the event listener
                    item.RumorsChanged -= OnItemChanged;

                    args = new CollectionChangedItemRemovedArgs(item, index);
                }

                // trigger the event
                OnCollectionChanged(args);

                return true;
            }
        }

        /// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</summary>
        /// <returns>The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</returns>
        public int Count
        {
            get
            {
                lock (SyncRoot)
                {
                    return _internalList.Count;
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> is
        ///     read-only.
        /// </summary>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only; otherwise, false.</returns>
        public bool IsReadOnly => false;

        #endregion

    }

}