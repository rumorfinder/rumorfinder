﻿namespace RumorFinder.Foundation
{

    public enum OperationStatus
    {

        Successful = 0,
        Unsuccessful = 1,
        Failed = 2

    }

}