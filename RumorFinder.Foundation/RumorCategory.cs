﻿namespace RumorFinder.Foundation
{

    /// <summary>
    ///     represents a category for text extracted from online articles
    /// </summary>
    public enum RumorCategory
    {

        General = 0,
        Specifications = 1,
        Features = 2,
        Design = 3,
        ReleaseDate = 4,
        Price = 5,
        Other = 6,
        NonRumor = 7

    }

}