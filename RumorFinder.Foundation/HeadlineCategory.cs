﻿namespace RumorFinder.Foundation
{

    /// <summary>
    ///     represents a category for an online headline
    /// </summary>
    public enum HeadlineCategory
    {

        Rumor = 0,
        Other = 1

    }

}