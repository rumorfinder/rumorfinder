﻿using System;

namespace RumorFinder.Foundation.Extensions.System
{

    internal static class TimeSpanExtensions
    {

        /// <summary>
        ///     divides a time span by the supplied denominator
        /// </summary>
        /// <param name="span"></param>
        /// <param name="denominator">the denominator to divide the span by, must be positive</param>
        /// <returns>a divided time span</returns>
        /// <exception cref="ArgumentException">throws when the denominator is less then 0</exception>
        public static TimeSpan DivideBy(this TimeSpan span, int denominator)
        {
            if (denominator < 0)
                throw new ArgumentException(nameof(denominator));
            long ticks = span.Ticks / denominator;
            return TimeSpan.FromTicks(ticks);
        }

    }

}