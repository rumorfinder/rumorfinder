﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RumorFinder.Foundation.Properties;

namespace RumorFinder.Foundation.Extensions.System
{

    internal static class StringExtensions
    {

        #region Static Fields and Constants

        private static readonly List<string> LineBreakRules;
        private static readonly List<string> NotALineBreakRules;

        #endregion

        #region Constructors

        static StringExtensions()
        {
            StringExtensions.LineBreakRules = JsonConvert.DeserializeObject<List<string>>(Resources.LineBreakRules);
            StringExtensions.NotALineBreakRules =
                JsonConvert.DeserializeObject<List<string>>(Resources.NotALineBreakRules);
        }

        #endregion

        /// <summary>
        ///     checks to see if the text ends with a line break
        /// </summary>
        /// <param name="txt"></param>
        /// <returns>true if the txt breaks, false otherwise</returns>
        public static bool EndsWithBreak(this string txt)
        {
            bool endsWithBreak =
                StringExtensions.LineBreakRules.Any(p => txt.EndsWith(p, StringComparison.CurrentCultureIgnoreCase));

            if (endsWithBreak == false) return false;

            endsWithBreak = StringExtensions
                            .NotALineBreakRules
                            .Count(p => txt.StartsWith(p, StringComparison.CurrentCultureIgnoreCase)) == 0;
            return endsWithBreak;
        }

    }

}