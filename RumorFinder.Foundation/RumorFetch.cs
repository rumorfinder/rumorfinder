﻿using Newtonsoft.Json;

namespace RumorFinder.Foundation
{

    public partial class Product
    {

        /// <summary>
        ///     represents an online fetch of rumors
        /// </summary>
        [JsonObject(MemberSerialization.OptIn)]
        private readonly struct RumorFetch
        {

            public static readonly RumorFetch Failed = new RumorFetch(Rumors.Empty, OperationStatus.Failed);

            public static readonly RumorFetch Unsuccessful = new RumorFetch(Rumors.Empty, OperationStatus.Unsuccessful);

            [JsonProperty]
            public Rumors Data { get; }

            [JsonProperty]
            public OperationStatus Status { get; }

            [JsonConstructor]
            public RumorFetch(Rumors data, OperationStatus status) =>
                (Data, Status) = (data, status);

        }

    }

}