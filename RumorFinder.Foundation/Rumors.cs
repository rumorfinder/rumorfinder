﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Newtonsoft.Json;
using OpenTextSummarizer;

namespace RumorFinder.Foundation
{

    /// <inheritdoc />
    /// <summary>
    ///     an immutable collection of rumors
    /// </summary>
    [JsonObject(MemberSerialization.Fields)]
    public class Rumors : IEnumerable<Rumor>
    {

        #region Static Fields and Constants

        public static readonly Rumors Empty = new Rumors();

        #endregion

        #region Fields

        [JsonProperty("internalDictionary")]
        private readonly ImmutableDictionary<RumorCategory, Rumor> _internalDictionary;

        #endregion

        #region Properties

        /// <summary>
        ///     return the Rumor object with the specified category, or null if none are found
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public Rumor this[RumorCategory category]
        {
            get
            {
                _internalDictionary.TryGetValue(category, out Rumor value);
                return value;
            }
        }

        public int Count => _internalDictionary.Count;

        public bool IsEmpty => this == Rumors.Empty;

        public int UpdatesCount => _internalDictionary.Values.Sum(r => r.UpdatesCount);

        public bool HasUpdates => UpdatesCount > 0;

        #endregion

        #region Constructors

        private Rumors() => _internalDictionary = ImmutableDictionary<RumorCategory, Rumor>.Empty;

        [JsonConstructor]
        private Rumors(ImmutableDictionary<RumorCategory, Rumor> internalDictionary) =>
            _internalDictionary = internalDictionary;

        #endregion

        /// <summary>
        ///     creates a rumors object from a range of rumor objects
        /// </summary>
        /// <param name="rumors"></param>
        /// <returns></returns>
        internal static Rumors FromRange(IEnumerable<Rumor> rumors)
        {
            Rumors current = Rumors.Empty;
            foreach (Rumor rumor in rumors)
            {
                current = current.AddOrReplace(rumor);
            }

            return current;
        }

        /// <summary>
        ///     Finds every piece of information in the updates source and adds it to the current collection of rumors.
        /// </summary>
        /// <param name="updateSource">The rumors collection to search for new information.</param>
        /// <param name="foundUpdates">Will hold the value of whether new information was found.</param>
        /// <returns>the foundUpdates Rumors object.</returns>
        internal Rumors UpdateFrom(Rumors updateSource, out bool foundUpdates)
        {
            Rumors newRumors = this;
            foundUpdates = false;

            // iterate through the rumors and seek to find new information
            foreach (Rumor source in updateSource)
            {
                if (source.IsEmpty)
                    continue;

                Rumor rumor = this[source.Category];
                bool rumorUpdated = false;

                // if the current rumors contain no rumors for the current category
                // we add it in its entirely to the collection
                if (rumor is null)
                    newRumors = newRumors.AddOrReplace(source);
                else // we search for new information in each category
                    newRumors = newRumors.AddOrReplace(rumor.UpdateFrom(source, out rumorUpdated));

                // make sure to check if we found new information
                foundUpdates |= rumorUpdated;
            }

            return newRumors;
        }

        /// <summary>
        ///     merges all the updates for each Rumor in this collection
        /// </summary>
        /// <returns>the foundUpdates Rumors object</returns>
        internal Rumors MergeUpdates()
        {
            Rumors newRumors = this;
            foreach (Rumor rumor in _internalDictionary.Values)
            {
                newRumors = newRumors.AddOrReplace(rumor.MergeUpdates());
            }

            return newRumors;
        }

        /// <summary>
        ///     summarizes all rumors in this collection
        /// </summary>
        /// <param name="args">the arguments to use when summarizing all the rumors</param>
        /// <returns>the foundUpdates Rumors object</returns>
        internal Rumors Summarize(SummarizerArguments args)
        {
            Rumors newRumors = this;
            foreach (Rumor rumor in _internalDictionary.Values)
            {
                newRumors = newRumors.AddOrReplace(rumor.Summarize(args));
            }

            return newRumors;
        }

        #region Private Helpers

        /// <summary>
        ///     wraps the internal dictionary in a new Rumors object
        /// </summary>
        /// <param name="internalDictionary"></param>
        /// <returns></returns>
        private Rumors Wrap(ImmutableDictionary<RumorCategory, Rumor> internalDictionary)
        {
            if (internalDictionary.IsEmpty)
                return Rumors.Empty;
            if (_internalDictionary.Equals(internalDictionary))
                return this;
            return new Rumors(internalDictionary);
        }

        #endregion

        #region Collection Methods

        /// <summary>
        ///     checks if a rumor with a corresponding category exists in this collection
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool Contains(RumorCategory category) =>
            _internalDictionary.Keys.Contains(category);

        /// <summary>
        ///     adds the Rumor to the collection
        /// </summary>
        /// <remarks>if a Rumor with the same category already exists in this collection, it will be replaced</remarks>
        /// <param name="rumor"></param>
        /// <returns>the foundUpdates object</returns>
        internal Rumors AddOrReplace(Rumor rumor) =>
            Wrap(_internalDictionary.SetItem(rumor.Category, rumor));

        /// <summary>
        ///     attempts to remove the Rumor with the corresponding category
        /// </summary>
        /// <param name="category"></param>
        /// <returns>the foundUpdates Rumors object</returns>
        internal Rumors Remove(RumorCategory category) => Wrap(_internalDictionary.Remove(category));

        #endregion

        #region IEnumerable Implementation

        /// <inheritdoc />
        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator<Rumor> IEnumerable<Rumor>.GetEnumerator() => _internalDictionary.Values.GetEnumerator();

        /// <inheritdoc />
        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator() => _internalDictionary.Values.GetEnumerator();

        #endregion

        #region Object Overrides

        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <param name="obj">The object to compare with the current object. </param>
        /// <returns>
        ///     <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        ///     <see langword="false" />.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is Rumors other) return _internalDictionary.Equals(other._internalDictionary);

            return false;
        }

        /// <summary>Serves as the default hash function. </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => _internalDictionary != null ? _internalDictionary.GetHashCode() : 0;

        #endregion

    }

}