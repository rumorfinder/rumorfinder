﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RumorFinder.Foundation.Extensions.System;
using RumorFinder.Foundation.Text;
using RumorFinder.Foundation.Util;
using RumorFinder.Foundation.Util.Data_Structures;
using RumorFinder.Foundation.Util.Progress_Reporters;

namespace RumorFinder.Foundation
{

    // The non-facing business logic for the product class is stored here
    public partial class Product
    {

        #region Fields

        [JsonProperty]
        private RumorFetch _lastFetch;

        [JsonProperty]
        private TimedCertificate _lastFetchCertificate = TimedCertificate.None;

        #endregion

        /// <summary>
        ///     The main operation of the Product class. Searches online for rumor articles, parsed the text from them, and returns
        ///     them in a categorized fashion
        /// </summary>
        /// <param name="client">the http client to perform the requests with</param>
        /// <param name="cancellationToken">the token used to cancel the operation</param>
        /// <param name="progress">the progress object used to report the progress fo the operation</param>
        /// <returns>a tuple that contains the operation status of the fetch, and the results of said fetch</returns>
        private async Task<RumorFetch> FetchRumorsAsync(
            HttpProgressClient client,
            CancellationToken cancellationToken,
            IProgress<float> progress)
        {
            //if the last fetch is still valid, we return it
            if (_lastFetchCertificate.IsValid)
                return _lastFetch;

            //create the progress reporter for the entire operation
            var stagedReporter = new StagedProgressReporter(progress, 2);

            //-----------------------------------------Stage 1--------------------------------------------
            //------------------search for applicable rumor articles about the product--------------------

            List<Uri> rumorArticles;
            try
            {
                rumorArticles = await GetRumorArticlesAsync(cancellationToken,
                                                            stagedReporter,
                                                            client).ConfigureAwait(false);
            }
            catch (SearchApiException)
            {
                return RumorFetch.Failed;
            }

            //if no articles are available at this time, then the operation was unsuccessful 
            if (rumorArticles.Count is 0)
            {
                RumorFetch unsuccessfulFetch = RumorFetch.Unsuccessful;
                SaveLastFetch(unsuccessfulFetch);
                return unsuccessfulFetch;
            }

            stagedReporter.CompleteCurrentStage(); //the first stage has ended

            //-----------------------------------------Stage 2--------------------------------------------
            //-----------------------extract the text from the wanted articles----------------------------

            List<ParsedArticle> parsedArticles = await ParseRumorArticlesAsync(rumorArticles,
                                                                               cancellationToken,
                                                                               stagedReporter,
                                                                               client).ConfigureAwait(false);

            //if no articles were successfully parsed, the operation has failed
            if (parsedArticles.Count is 0)
                return RumorFetch.Failed;

            stagedReporter.CompleteCurrentStage(); //the second stage has ended

            //-----------------------------------------Stage 3--------------------------------------------
            //---------------------prepare the found information for user consumption---------------------

            var filteredRumors = Filter(parsedArticles);
            var categorizedRumors = Categorize(filteredRumors);
            var summarizedRumors = Summarize(categorizedRumors);

            var fetch = new RumorFetch(summarizedRumors, Evaluate(summarizedRumors));
            SaveLastFetch(fetch);
            return fetch;
        }

        /// <summary>
        ///     searches the internet for rumor articles about the product and returns all applicable results
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="progress"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private async Task<List<Uri>> GetRumorArticlesAsync(
            CancellationToken cancellationToken,
            IProgress<float> progress,
            HttpProgressClient client)
        {
            //configure the api request
            string searchQuery = string.Format(Product.SearchQueryFormat, Name);
            var apiRequest = SearchApi.GetSearchResultsAsync(searchQuery,
                                                             cancellationToken,
                                                             progress,
                                                             client);

            IEnumerable<SearchResult> searchResults = await apiRequest.ConfigureAwait(false);

            //extract the wanted articles from the returned results, cast to list to ensure multiple enumerations
            return Extract(searchResults).ToList();
        }

        /// <summary>
        ///     parses the text from the given rumor articles
        /// </summary>
        /// <param name="rumorArticles"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="progress"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private async Task<List<ParsedArticle>> ParseRumorArticlesAsync(
            IEnumerable<Uri> rumorArticles,
            CancellationToken cancellationToken,
            IProgress<float> progress,
            HttpProgressClient client)
        {
            var apiRequest = ArticleParserApi.ParseArticlesAsync(rumorArticles,
                                                                 cancellationToken,
                                                                 progress,
                                                                 client);
            //extract the text from the rumors articles
            IEnumerable<ParsedArticle?> articles = await apiRequest.ConfigureAwait(false);

            //select the successfully parsed articles
            IEnumerable<ParsedArticle> successfullyParsedArticles = from article in articles
                                                                    where article.HasValue
                                                                    select article.Value;

            //cast to list to ensure multiple enumerations
            return successfullyParsedArticles.ToList();
        }

        #region Private Helpers

        /// <summary>
        ///     extracts the articles uris from the search results
        /// </summary>
        /// <param name="searchResults"></param>
        /// <returns></returns>
        private IEnumerable<Uri> Extract(IEnumerable<SearchResult> searchResults)
        {
            return from searchResult in searchResults
                   where !Product.BlackListedSources.Any(bs => bs.Host.Equals(searchResult.Uri.Host)) &&
                         Classifier.ClassifyHeadline(searchResult.Headline, Name) == HeadlineCategory.Rumor
                   select searchResult.Uri;
        }

        /// <summary>
        ///     filter all the extracted paragraphs and assign them to a category
        /// </summary>
        /// <param name="parsedArticles"></param>
        /// <returns></returns>
        private IEnumerable<(RumorCategory, string)> Filter(IEnumerable<ParsedArticle> parsedArticles) =>
            from parsedArticle in parsedArticles
            from paragraph in parsedArticle.Paragraphs
            where paragraph.Length > 0 && paragraph.EndsWithBreak()
            let category = Classifier.ClassifyRumor(paragraph, Name)
            where category != RumorCategory.NonRumor
            select (category, paragraph);

        /// <summary>
        ///     categorize the paragraph by grouping them into Rumor objects
        /// </summary>
        /// <param name="filteredRumors"></param>
        /// <returns></returns>
        private IEnumerable<Rumor> Categorize(IEnumerable<(RumorCategory category, string paragraph)> filteredRumors) =>
            from tuple in filteredRumors
            group tuple.paragraph by tuple.category
            into paragraphs
            select new Rumor(paragraphs.Key, paragraphs);

        /// <summary>
        ///     summarize the categorizedRumors and return them in a Rumors object
        /// </summary>
        /// <param name="categorizedRumors"></param>
        /// <returns></returns>
        private Rumors Summarize(IEnumerable<Rumor> categorizedRumors)
        {
            Rumors rumorsCollection = Rumors.FromRange(categorizedRumors);

            return rumorsCollection.Summarize(Product.InternalArguments);
        }

        /// <summary>
        ///     evaluate the success of the operation
        /// </summary>
        /// <param name="rumors"></param>
        /// <returns></returns>
        private OperationStatus Evaluate(Rumors rumors) =>
            rumors.IsEmpty ? OperationStatus.Unsuccessful : OperationStatus.Successful;

        /// <summary>
        ///     renew the certificate and save the last fetched categorizedRumors
        /// </summary>
        /// <param name="fetch"></param>
        private void SaveLastFetch(RumorFetch fetch)
        {
            //calculate the renewal span according to the status and renew the certificate with it
            TimeSpan renewalSpan = fetch.Status == OperationStatus.Successful
                ? Product.RumorsValidationTimeSpan
                : Product.RumorsValidationTimeSpan.DivideBy(2);
            _lastFetchCertificate = _lastFetchCertificate.Renew(renewalSpan);

            //save the last fetch
            _lastFetch = fetch;
        }

        #endregion

    }

}