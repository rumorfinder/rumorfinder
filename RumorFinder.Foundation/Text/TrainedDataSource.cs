﻿using NClassifier.Bayesian;
using Newtonsoft.Json;

namespace RumorFinder.Foundation.Text
{

    /// <summary>
    ///     represnts a trained data source that is deserialized from the supplied json string
    /// </summary>
    internal class TrainedDataSource
    {

        #region Fields

        private readonly object _dataLock = new object();

        private readonly string _serializedDataSource;
        private SimpleWordsDataSource _dataSource;

        #endregion

        #region Properties

        private SimpleWordsDataSource DataSource
        {
            get
            {
                if (_dataSource != null) return _dataSource;
                lock (_dataLock)
                {
                    _dataSource = _dataSource ??
                                  JsonConvert.DeserializeObject<SimpleWordsDataSource>(_serializedDataSource);
                }

                return _dataSource;
            }
        }

        #endregion

        #region Constructors

        public TrainedDataSource(string jsonDataSource) => _serializedDataSource = jsonDataSource;

        #endregion

        /// <summary>
        ///     converts the trained data source to a <code>SimpleWordsDataSource</code>
        /// </summary>
        /// <param name="tds">the trained data source to convert</param>
        public static implicit operator SimpleWordsDataSource(TrainedDataSource tds) => tds.DataSource;

    }

}