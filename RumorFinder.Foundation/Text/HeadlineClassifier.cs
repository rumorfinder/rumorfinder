﻿using System;
using NClassifier.Bayesian;
using RumorFinder.Foundation.Properties;

namespace RumorFinder.Foundation.Text
{

    public static partial class Classifier
    {

        /// <summary>
        ///     represents a headline classifier that uses a trained bayesian classifier
        /// </summary>
        private static class HeadlineClassifier
        {

            #region Static Fields and Constants

            private const string ProductNameId = "productname ";

            private static readonly TrainedDataSource DataSource =
                new TrainedDataSource(Resources.HeadlinesTrainedDataSource);

            private static readonly BayesianClassifier RumorHeadlineClassifier;

            #endregion

            #region Constructors

            static HeadlineClassifier()
            {
                var ds = new SimpleWordsDataSource(HeadlineClassifier.DataSource);
                HeadlineClassifier.RumorHeadlineClassifier = new BayesianClassifier(ds);
            }

            #endregion

            /// <summary>
            ///     classifies an online headline
            /// </summary>
            /// <param name="headline">headline to classify</param>
            /// <param name="productName">the product name associated to the headline</param>
            /// <returns>the category of the given headline</returns>
            public static HeadlineCategory ClassifyOnlineHeadline(string headline, string productName)
            {
                if (string.IsNullOrEmpty(headline) ||
                    headline.IndexOf(productName, StringComparison.CurrentCultureIgnoreCase) < 0)
                    return HeadlineCategory.Other;
                string newHeadline =
                    headline.ToLower().Replace(productName.ToLower(), HeadlineClassifier.ProductNameId);
                return HeadlineClassifier.RumorHeadlineClassifier.IsMatch(newHeadline)
                    ? HeadlineCategory.Rumor
                    : HeadlineCategory.Other;
            }

        }

    }

}