﻿using System;

namespace RumorFinder.Foundation.Text
{

    /// <summary>
    ///     a text classification interface with interchangeable methods of classification
    /// </summary>
    public static partial class Classifier
    {

        // ReSharper disable once TypeParameterCanBeVariant
        public delegate TCategory Classification<TCategory>(string text, string productName) where TCategory : Enum;

        #region Static Fields and Constants

        private static readonly object HeadlineClassificationLock = new object();
        private static Classification<HeadlineCategory> HeadlineClassification;

        private static readonly object RumorClassificationLock = new object();
        private static Classification<RumorCategory> RumorClassification;

        #endregion

        #region Constructors

        /// <summary>
        ///     set the default classification methods
        /// </summary>
        static Classifier()
        {
            SetHeadlineClassificationMethod(HeadlineClassifier.ClassifyOnlineHeadline, false);
            SetRumorClassificationMethod((rumor, productName) => RumorCategory.General, true);
        }

        #endregion

        /// <summary>
        ///     changes the default headline classification method of the classifier
        /// </summary>
        /// <param name="classificationMethod">the method to be used by the classifier</param>
        /// <param name="isSynchronized">if the given method is synchronized</param>
        public static void SetHeadlineClassificationMethod(
            Classification<HeadlineCategory> classificationMethod,
            bool isSynchronized)
        {
            Classifier.HeadlineClassification = isSynchronized
                ? classificationMethod
                : classificationMethod.WithLock(Classifier.HeadlineClassificationLock);
        }

        /// <summary>
        ///     classifies an online headline
        /// </summary>
        /// <param name="headline">headline to classify</param>
        /// <param name="productName">the product name associated to the headline</param>
        /// <returns>the category of the given headline</returns>
        public static HeadlineCategory ClassifyHeadline(string headline, string productName) =>
            Classifier.HeadlineClassification(headline, productName);

        /// <summary>
        ///     changes the default rumor classification method of the classifier
        /// </summary>
        /// <param name="classificationMethod">the method to be used by the classifier</param>
        /// <param name="isSynchronized">if the given method is synchronized</param>
        public static void SetRumorClassificationMethod(
            Classification<RumorCategory> classificationMethod,
            bool isSynchronized)
        {
            Classifier.RumorClassification = isSynchronized
                ? classificationMethod
                : classificationMethod.WithLock(Classifier.RumorClassificationLock);
        }

        /// <summary>
        ///     classifies a paragraph from an online rumor article
        /// </summary>
        /// <param name="rumor">the rumor paragraph to classify</param>
        /// <param name="productName">the product name associated to the online article</param>
        /// <returns>the category of the given rumor paragraph</returns>
        public static RumorCategory ClassifyRumor(string rumor, string productName) =>
            Classifier.RumorClassification(rumor, productName);

        #region Private Helpers

        /// <summary>
        ///     synchronizes a classification delegate
        /// </summary>
        /// <typeparam name="TCategory">the category for the classification delegate</typeparam>
        /// <param name="classificationDelegate">the delegate to synchronize</param>
        /// <param name="lockObj">the object to use for the synchronization </param>
        /// <returns>a synchronized wrapper delegate</returns>
        private static Classification<TCategory> WithLock<TCategory>(
            this Classification<TCategory> classificationDelegate,
            object lockObj) where TCategory : Enum
        {
            return (text, productName) =>
            {
                lock (lockObj)
                {
                    return classificationDelegate(text, productName);
                }
            };
        }

        #endregion

    }

}