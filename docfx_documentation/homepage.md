﻿## Welcome to the documentation about RumorFinder.Foundation
This project contains most of the business logic for the RumorFinder app.

There are a number of places to start reading the documentation from:
- The Overview section
> It is recommended to start with reading the [**About The Search Algorithm**](SearchAlgorithm.md) article.
- The Refrence section
>It is recommended to start with the [`Product`](xref:RumorFinder.Foundation.Product) Class.