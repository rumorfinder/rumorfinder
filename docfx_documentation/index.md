
# RumorFinder Documentation
This websire contains documentation for the RumorFinder project. 
You can find documentation for four projects:

## 1. [The RumorFinder project](xref:RumorFinder)
This is a Xamarin.iOS prject that contains the code for the app.
## 2. [The RumorFinder.Foundation project](homepage.md)
This is a .NET standard class library that contains the business logic of the app (mainly the code that search for rumors about products).
## 3. [The RumorFinder.NativeHttp project](xref:RumorFinder.NativeHttp)
This is a multi target class library that wraps the NSUrlSession library so that it can be used with the .NET HttpClient class and report the progress of ongoing HTTP requests (which currently isn't available with the HttpClient class).
## 4. [The RumorFinder Server project](server/index.html)
This project contains code for a backend server used by the Rumorfinder iOS app.
The server contains two functions, a web parser and a recommender system, that are written in python (and a little js).
