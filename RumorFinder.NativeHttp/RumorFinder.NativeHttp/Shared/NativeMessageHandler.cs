﻿using System;
using System.Net.Http;

namespace RumorFinder.NativeHttp
{

    /// <summary>
    ///     The class that will be visible to .Net Standard projects instead of the platform specific classes.
    /// </summary>
    public class NativeMessageHandler : HttpClientHandler
    {

        #region Static Fields and Constants

        private const string WrongVersion =
            "You're referencing the Portable version in your App - you need to reference the platform (iOS/Android/Windows) version";

        #endregion

        public void RegisterProgressCallback(HttpRequestMessage request, IProgress<float> progress) =>
            throw new Exception(NativeMessageHandler.WrongVersion);

    }

}