﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Foundation;

// ReSharper disable once CheckNamespace
namespace RumorFinder.NativeHttp
{

    /// <summary>
    ///     a native http message handler that uses the NSUrlSession library from Apple.
    /// </summary>
    public partial class NativeMessageHandler : HttpClientHandler
    {

        #region Static Fields and Constants

        private static readonly Dictionary<string, string> HeaderSeparators = new Dictionary<string, string>
        {
            ["User-Agent"] = " ",
            ["Server"] = " "
        };

        #endregion

        #region Fields

        private readonly ConcurrentDictionary<NSUrlSessionTask, InflightData> _inflightRequests;
        private readonly ConcurrentDictionary<HttpRequestMessage, IProgress<float>> _registeredProgressCallbacks;

        private readonly NSUrlSession _session;

        #endregion

        #region Constructors

        public NativeMessageHandler()
        {
            _session = NSUrlSession.FromConfiguration(NSUrlSessionConfiguration.DefaultSessionConfiguration,
                                                      (INSUrlSessionDelegate) new DataTaskDelegate(this), null);
            _inflightRequests = new ConcurrentDictionary<NSUrlSessionTask, InflightData>();
            _registeredProgressCallbacks = new ConcurrentDictionary<HttpRequestMessage, IProgress<float>>();
        }

        #endregion

        /// <summary>
        ///     Register a progress callback to be used to report progress about the completion of the given request.
        /// </summary>
        /// <param name="request">the request to track progress about.</param>
        /// <param name="progress">the object used to report progress abut the request.</param>
        public void RegisterProgressCallback(HttpRequestMessage request, IProgress<float> progress) =>
            _registeredProgressCallbacks[request] = progress;

        /// <summary>
        ///     returns and removes the associated progress callback with the given request.
        /// </summary>
        /// <param name="request">the request to get the progress callback for</param>
        /// <returns>the progress callback for the request, or null if it doesn't exists.</returns>
        private IProgress<float> GetAndRemoveCallback(HttpRequestMessage request)
        {
            _registeredProgressCallbacks.TryRemove(request, out IProgress<float> progress);
            return progress;
        }

        /// <summary>
        ///     creates a url request from the .NET http request.
        /// </summary>
        /// <param name="request">the request to transfer its contents to the url request.</param>
        /// <returns>a url request that contains the same information as the .NET http request.</returns>
        private async Task<NSUrlRequest> CreateUrlRequestFrom(HttpRequestMessage request)
        {
            var content = Array.Empty<byte>();
            var headers = request.Headers as IEnumerable<KeyValuePair<string, IEnumerable<string>>>;

            //read the content into a buffer and combine the headers
            if (request.Content != null)
            {
                content = await request.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
                headers = headers.Union(request.Content.Headers).ToArray();
            }

            //create the url request
            var urlRequest = new NSMutableUrlRequest
            {
                AllowsCellularAccess = true,
                HttpMethod = request.Method.ToString().ToUpperInvariant(),
                Url = NSUrl.FromString(request.RequestUri.AbsoluteUri),
                Headers = headers.Aggregate(new NSMutableDictionary(), (dict, header) =>
                {
                    dict.Add(new NSString(header.Key),
                             new NSString(string.Join(GetHeaderSeparator(header.Key), header.Value)));
                    return dict;
                })
            };

            if (content.Length > 0)
                urlRequest.Body = NSData.FromArray(content);

            return urlRequest;

            //returns the appropriate header separator for the header key
            string GetHeaderSeparator(string key)
            {
                if (!NativeMessageHandler.HeaderSeparators.TryGetValue(key, out string separator))
                {
                    separator = ",";
                }

                return separator;
            }
        }

        /// <summary>
        ///     Creates an instance of  <see cref="T:System.Net.Http.HttpResponseMessage" /> based on the information provided
        ///     in the <see cref="T:System.Net.Http.HttpRequestMessage" /> as an operation that will not block.
        /// </summary>
        /// <param name="request">The HTTP request message.</param>
        /// <param name="cancellationToken">A cancellation token to cancel the operation.</param>
        /// <returns>The task object representing the asynchronous operation.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            //create the data task
            NSUrlRequest req = await CreateUrlRequestFrom(request).ConfigureAwait(false);
            NSUrlSessionDataTask dataTask = _session.CreateDataTask(req);

            var completionSource = new TaskCompletionSource<HttpResponseMessage>();

            //connect the cancellation of the token to the cancellation of the task
            cancellationToken.Register(() =>
            {
                completionSource.TrySetCanceled(cancellationToken);
                dataTask.Cancel();
            });

            //create the data for the task to be used in the processing of the request
            _inflightRequests[dataTask] = new InflightData
            {
                CancellationToken = cancellationToken,
                CompletionSource = completionSource,
                RequestMessage = request,
                Progress = GetAndRemoveCallback(request)
            };

            //resume the tasks
            if (dataTask.State is NSUrlSessionTaskState.Suspended)
                dataTask.Resume();
            //await the task
            return await completionSource.Task.ConfigureAwait(false);
        }

    }

}