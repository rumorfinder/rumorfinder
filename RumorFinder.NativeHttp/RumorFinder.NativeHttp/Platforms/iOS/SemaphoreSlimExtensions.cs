﻿using System.Threading;

namespace RumorFinder.NativeHttp.Platforms.iOS
{

    internal static class SemaphoreSlimExtensions
    {

        /// <summary>
        ///     Attempts to release the semaphore.
        /// </summary>
        /// <param name="semaphore">The semaphore to release.</param>
        /// <param name="releaseCount">the release count for the release action.</param>
        /// <returns>true if the semaphore was released successfully, false otherwise.</returns>
        public static bool TryRelease(this SemaphoreSlim semaphore, int releaseCount = 1)
        {
            try
            {
                semaphore.Release(releaseCount);
                return true;
            }
            catch (SemaphoreFullException)
            {
                return false;
            }
        }

    }

}