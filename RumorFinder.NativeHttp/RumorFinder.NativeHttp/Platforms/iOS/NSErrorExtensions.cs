﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using CoreFoundation;
using Foundation;

namespace RumorFinder.NativeHttp.Platforms.iOS
{

    static class NsErrorExtensions
    {

        /// <summary>
        ///     converts the given ns error to a .net exception
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static Exception AsException(this NSError error)
        {
            var webExceptionStatus = WebExceptionStatus.UnknownError;

            var innerException = new NSErrorException(error);

            // errors that exists in both share the same error code, so we can use a single switch/case
            if (error.Domain == NSError.NSUrlErrorDomain || error.Domain == NSError.CFNetworkErrorDomain)
            {
                // Parse the enum into a web exception status or exception. Some
                // of these values don't necessarily translate completely to
                // what WebExceptionStatus supports, so made some best guesses
                // here.  For your reading pleasure, compare these:
                //
                // Apple docs: https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Constants/index.html#//apple_ref/doc/constant_group/URL_Loading_System_Error_Codes
                // .NET docs: http://msdn.microsoft.com/en-us/library/system.net.webexceptionstatus(v=vs.110).aspx
                switch ((NSUrlError) (long) error.Code)
                {
                    case NSUrlError.Cancelled:
                    case NSUrlError.UserCancelledAuthentication:
                    case (NSUrlError) NSNetServicesStatus.CancelledError:
                        // No more processing is required so just return.
                        return new OperationCanceledException(error.LocalizedDescription, innerException);
                    case NSUrlError.BadURL:
                    case NSUrlError.UnsupportedURL:
                    case NSUrlError.CannotConnectToHost:
                    case NSUrlError.ResourceUnavailable:
                    case NSUrlError.NotConnectedToInternet:
                    case NSUrlError.UserAuthenticationRequired:
                    case NSUrlError.InternationalRoamingOff:
                    case NSUrlError.CallIsActive:
                    case NSUrlError.DataNotAllowed:
                    case (NSUrlError) CFNetworkErrors.Socks5BadCredentials:
                    case (NSUrlError) CFNetworkErrors.Socks5UnsupportedNegotiationMethod:
                    case (NSUrlError) CFNetworkErrors.Socks5NoAcceptableMethod:
                    case (NSUrlError) CFNetworkErrors.HttpAuthenticationTypeUnsupported:
                    case (NSUrlError) CFNetworkErrors.HttpBadCredentials:
                    case (NSUrlError) CFNetworkErrors.HttpBadURL:
                        webExceptionStatus = WebExceptionStatus.ConnectFailure;
                        break;
                    case NSUrlError.TimedOut:
                    case (NSUrlError) CFNetworkErrors.NetServiceTimeout:
                        webExceptionStatus = WebExceptionStatus.Timeout;
                        break;
                    case NSUrlError.CannotFindHost:
                    case NSUrlError.DNSLookupFailed:
                    case (NSUrlError) CFNetworkErrors.HostNotFound:
                    case (NSUrlError) CFNetworkErrors.NetServiceDnsServiceFailure:
                        webExceptionStatus = WebExceptionStatus.NameResolutionFailure;
                        break;
                    case NSUrlError.DataLengthExceedsMaximum:
                        webExceptionStatus = WebExceptionStatus.MessageLengthLimitExceeded;
                        break;
                    case NSUrlError.NetworkConnectionLost:
                    case (NSUrlError) CFNetworkErrors.HttpConnectionLost:
                        webExceptionStatus = WebExceptionStatus.ConnectionClosed;
                        break;
                    case NSUrlError.HTTPTooManyRedirects:
                    case NSUrlError.RedirectToNonExistentLocation:
                    case (NSUrlError) CFNetworkErrors.HttpRedirectionLoopDetected:
                        webExceptionStatus = WebExceptionStatus.ProtocolError;
                        break;
                    case NSUrlError.RequestBodyStreamExhausted:
                    case (NSUrlError) CFNetworkErrors.SocksUnknownClientVersion:
                    case (NSUrlError) CFNetworkErrors.SocksUnsupportedServerVersion:
                    case (NSUrlError) CFNetworkErrors.HttpParseFailure:
                        webExceptionStatus = WebExceptionStatus.SendFailure;
                        break;
                    case NSUrlError.BadServerResponse:
                    case NSUrlError.ZeroByteResource:
                    case NSUrlError.CannotDecodeRawData:
                    case NSUrlError.CannotDecodeContentData:
                    case NSUrlError.CannotParseResponse:
                    case NSUrlError.FileDoesNotExist:
                    case NSUrlError.FileIsDirectory:
                    case NSUrlError.NoPermissionsToReadFile:
                    case NSUrlError.CannotLoadFromNetwork:
                    case NSUrlError.CannotCreateFile:
                    case NSUrlError.CannotOpenFile:
                    case NSUrlError.CannotCloseFile:
                    case NSUrlError.CannotWriteToFile:
                    case NSUrlError.CannotRemoveFile:
                    case NSUrlError.CannotMoveFile:
                    case NSUrlError.DownloadDecodingFailedMidStream:
                    case NSUrlError.DownloadDecodingFailedToComplete:
                    case (NSUrlError) CFNetworkErrors.Socks4RequestFailed:
                    case (NSUrlError) CFNetworkErrors.Socks4IdentdFailed:
                    case (NSUrlError) CFNetworkErrors.Socks4IdConflict:
                    case (NSUrlError) CFNetworkErrors.Socks4UnknownStatusCode:
                    case (NSUrlError) CFNetworkErrors.Socks5BadState:
                    case (NSUrlError) CFNetworkErrors.Socks5BadResponseAddr:
                    case (NSUrlError) CFNetworkErrors.CannotParseCookieFile:
                    case (NSUrlError) CFNetworkErrors.NetServiceUnknown:
                    case (NSUrlError) CFNetworkErrors.NetServiceCollision:
                    case (NSUrlError) CFNetworkErrors.NetServiceNotFound:
                    case (NSUrlError) CFNetworkErrors.NetServiceInProgress:
                    case (NSUrlError) CFNetworkErrors.NetServiceBadArgument:
                    case (NSUrlError) CFNetworkErrors.NetServiceInvalid:
                        webExceptionStatus = WebExceptionStatus.ReceiveFailure;
                        break;
                    case NSUrlError.SecureConnectionFailed:
                        webExceptionStatus = WebExceptionStatus.SecureChannelFailure;
                        break;
                    case NSUrlError.ServerCertificateHasBadDate:
                    case NSUrlError.ServerCertificateHasUnknownRoot:
                    case NSUrlError.ServerCertificateNotYetValid:
                    case NSUrlError.ServerCertificateUntrusted:
                    case NSUrlError.ClientCertificateRejected:
                    case NSUrlError.ClientCertificateRequired:
                        webExceptionStatus = WebExceptionStatus.TrustFailure;
                        break;
                    case (NSUrlError) CFNetworkErrors.HttpProxyConnectionFailure:
                    case (NSUrlError) CFNetworkErrors.HttpBadProxyCredentials:
                    case (NSUrlError) CFNetworkErrors.PacFileError:
                    case (NSUrlError) CFNetworkErrors.PacFileAuth:
                    case (NSUrlError) CFNetworkErrors.HttpsProxyConnectionFailure:
                    case (NSUrlError) CFNetworkErrors.HttpsProxyFailureUnexpectedResponseToConnectMethod:
                        webExceptionStatus = WebExceptionStatus.RequestProhibitedByProxy;
                        break;
                }
            }

            var webException = new WebException(error.LocalizedDescription, innerException, webExceptionStatus,
                                                response: null);

            //always return a http request exception because that is what the client expects
            return new HttpRequestException(error.LocalizedDescription, webException);
        }

    }

}