﻿using System;
using System.IO.Pipelines;
using System.Net;
using System.Net.Http;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using Foundation;
using RumorFinder.NativeHttp.Platforms.iOS;

// ReSharper disable once CheckNamespace
namespace RumorFinder.NativeHttp
{

    public partial class NativeMessageHandler
    {

        /// <summary>
        ///     represents a session delegate that makes the conversion between iOS Data tasks and .NET http responses.
        /// </summary>
        private class DataTaskDelegate : NSUrlSessionDataDelegate
        {

            #region Fields

            private readonly NativeMessageHandler _sessionHandler;

            #endregion

            #region Constructors

            public DataTaskDelegate(NativeMessageHandler sessionHandler) => _sessionHandler = sessionHandler;

            #endregion

            /// <summary>
            ///     returns the inflight data associated with the task.
            /// </summary>
            /// <param name="dataTask"></param>
            /// <returns>The inflight data for the task, or null if none is found</returns>
            private InflightData GetInflightDataFor(NSUrlSessionTask dataTask)
            {
                _sessionHandler._inflightRequests.TryGetValue(dataTask, out InflightData data);
                return data;
            }

            /// <summary>
            ///     Remove the inflight data for the task
            /// </summary>
            /// <param name="dataTask"></param>
            private void RemoveInflightDataFor(NSUrlSessionTask dataTask)
            {
                //try and remove from the internal dictionary
                _sessionHandler._inflightRequests.TryRemove(dataTask, out _);

                //dispose of the task
                dataTask?.Dispose();
            }

            /// <summary>
            ///     Sets the http response for the inflight data if it hasn't already been set.
            /// </summary>
            /// <param name="data"></param>
            private void TrySetResponseFor(InflightData data)
            {
                if (data.ResponseSent)
                    return;

                if (data.CompletionSource.Task.IsCompleted)
                    return;

                //set the response
                HttpResponseMessage httpResponse = data.ResponseMessage;

                data.ResponseSent = true;

                // EVIL HACK: having TrySetResult inline was blocking the request from completing
                Task.Run(() => data.CompletionSource.TrySetResult(httpResponse));
            }

            /// <summary>Periodically informs the delegate of the progress of sending body content to the server.</summary>
            /// <param name="session">The session containing the data task.</param>
            /// <param name="task">The data task.</param>
            /// <param name="bytesSent">The number of bytes sent since the last time this delegate method was called.</param>
            /// <param name="totalBytesSent">The total number of bytes sent so far.</param>
            /// <param name="totalBytesExpectedToSend">The expected length of the body data.</param>
            public override void DidSendBodyData(
                NSUrlSession session,
                NSUrlSessionTask task,
                long bytesSent,
                long totalBytesSent,
                long totalBytesExpectedToSend)
            {
                InflightData inflightData = GetInflightDataFor(task);

                // return if the request has completed(with error) or has canceled or it has no content
                if (inflightData is null || inflightData.Done || inflightData.RequestMessage.Content is null)
                    return;

                try
                {
                    // report the overall upload progress 
                    float uploadProgress = 0.5f * totalBytesSent / totalBytesExpectedToSend;
                    inflightData.UploadProgress = uploadProgress;
                    inflightData.Progress?.Report(uploadProgress);
                }
                catch (Exception e)
                {
                    // set the exception that occured
                    inflightData.CompletionSource.TrySetException(e);
                    inflightData.ResponseContent.SetReadException(ExceptionDispatchInfo.Capture(e));

                    // remove the data for the faulted operation
                    RemoveInflightDataFor(task);
                }
                finally
                {
                    // release the lock
                    inflightData.Lock.TryRelease();
                }
            }

            /// <summary>
            ///     Tells the delegate that the data task received the initial reply (headers) from the server.
            /// </summary>
            /// <param name="session">The session containing the data task that received an initial reply.</param>
            /// <param name="dataTask">The data task that received an initial reply.</param>
            /// <param name="response">A URL response object populated with headers.</param>
            /// <param name="completionHandler">
            ///     A completion sessionHandler that your code calls to continue a transfer, passing a
            ///     URLSession.ResponseDisposition constant to indicate whether the transfer should continue as a data task or should
            ///     become a download task.
            /// </param>
            public override async void DidReceiveResponse(
                NSUrlSession session,
                NSUrlSessionDataTask dataTask,
                NSUrlResponse response,
                Action<NSUrlSessionResponseDisposition> completionHandler)
            {
                InflightData inflightData = GetInflightDataFor(dataTask);

                //return if the request has completed(with error) or has canceled
                if (inflightData is null || inflightData.Done)
                    return;

                NSUrlSessionResponseDisposition responseDisposition = NSUrlSessionResponseDisposition.Allow;

                try
                {
                    //synchronise the operation
                    await inflightData.Lock.WaitAsync(inflightData.CancellationToken).ConfigureAwait(false);

                    //extract the data from the url response
                    var urlResponse = (NSHttpUrlResponse) response;
                    var status = (int) urlResponse.StatusCode;

                    //extract the expected length
                    long? responseLength = null;
                    if (urlResponse.ExpectedContentLength > 0)
                        responseLength = urlResponse.ExpectedContentLength;

                    //create the Http content with the response's pipe reader
                    var content = new PipeReaderContent(inflightData.ContentPipe.Reader, responseLength,
                                                        inflightData.CancellationToken);

                    // NB: The double cast is because of a Xamarin compiler bug
                    var httpResponse = new HttpResponseMessage((HttpStatusCode) status)
                    {
                        Content = content,
                        RequestMessage = inflightData.RequestMessage
                    };
                    httpResponse.RequestMessage.RequestUri = new Uri(urlResponse.Url.AbsoluteString);

                    //extract the headers from the url response
                    foreach (var header in urlResponse.AllHeaderFields)
                    {
                        // NB: Cocoa trolling us so hard by giving us back dummy dictionary entries
                        if (header.Key == null || header.Value == null) continue;

                        httpResponse.Headers.TryAddWithoutValidation(header.Key.ToString(), header.Value.ToString());
                        httpResponse.Content.Headers.TryAddWithoutValidation(header.Key.ToString(),
                                                                             header.Value.ToString());
                    }

                    // save the response information
                    inflightData.ResponseContent = content;
                    inflightData.ResponseMessage = httpResponse;

                    // We don't want to send the response back to the task just yet.  Because we want to mimic .NET behavior
                    // as much as possible.  When the response is sent back in .NET, the content stream is ready to read or the
                    // request has completed, because of this we want to send back the response in DidReceiveData or DidCompleteWithError
                    if (dataTask.State == NSUrlSessionTaskState.Suspended)
                        dataTask.Resume();
                }
                catch (Exception ex)
                {
                    //set the exception
                    inflightData.CompletionSource.TrySetException(ex);

                    //remove the data
                    RemoveInflightDataFor(dataTask);

                    //cancel the task
                    responseDisposition = NSUrlSessionResponseDisposition.Cancel;
                }
                finally
                {
                    //cancel the task if there was an error or allow it to proceed
                    completionHandler(responseDisposition);

                    //release the lock
                    inflightData.Lock.TryRelease();
                }
            }

            /// <summary>Tells the delegate that the data task has received some of the expected data.</summary>
            /// <param name="session">The session containing the data task that provided data.</param>
            /// <param name="dataTask">The data task that provided data.</param>
            /// <param name="data">A data object containing the transferred data.</param>
            public override async void DidReceiveData(NSUrlSession session, NSUrlSessionDataTask dataTask, NSData data)
            {
                InflightData inflightData = GetInflightDataFor(dataTask);

                //return if the request has completed(with error) or has canceled
                if (inflightData is null || inflightData.Done)
                    return;

                try
                {
                    // synchronise the operation
                    await inflightData.Lock.WaitAsync(inflightData.CancellationToken).ConfigureAwait(false);

                    PipeWriter writer = inflightData.ContentPipe.Writer;

                    int dataLength = (int) data.Length;

                    // retrieve a buffer from the pipe
                    Memory<byte> buffer = writer.GetMemory(dataLength);

                    // write the data to the buffer
                    data.WriteTo(buffer);

                    // signal to the pipe what data has been written
                    writer.Advance(dataLength);

                    // make the data available to read
                    await writer.FlushAsync(inflightData.CancellationToken);

                    // make the response available to read now because there is finally data in the pipe
                    TrySetResponseFor(inflightData);

                    inflightData.BytesWritten += dataLength;

                    // report the overall progress 
                    if (dataTask.BytesExpectedToReceive > 0)
                    {
                        float progress = 1f * inflightData.BytesWritten / dataTask.BytesExpectedToReceive;
                        
                        // account for upload progress if there was any
                        if (inflightData.UploadProgress > 0)
                        {
                            progress = 0.5f + 0.5f * progress;
                        }

                        inflightData.Progress?.Report(progress);
                    }
                }
                catch (Exception e)
                {
                    // set the exception that occured
                    inflightData.CompletionSource.TrySetException(e);
                    inflightData.ResponseContent.SetReadException(ExceptionDispatchInfo.Capture(e));

                    // remove the data for the faulted operation
                    RemoveInflightDataFor(dataTask);
                }
                finally
                {
                    // release the lock
                    inflightData.Lock.TryRelease();
                }
            }

            /// <summary>Tells the delegate that the task finished transferring data.</summary>
            /// <param name="session">The session containing the task whose request finished transferring data.</param>
            /// <param name="task">The task whose request finished transferring data.</param>
            /// <param name="error">If an error occurred, an error object indicating how the transfer failed, otherwise null.</param>
            public override async void DidCompleteWithError(NSUrlSession session, NSUrlSessionTask task, NSError error)
            {
                InflightData inflightData = GetInflightDataFor(task);

                //return if the request has completed(with error) or has canceled
                if (inflightData is null || inflightData.Done)
                    return;

                try
                {
                    // make the operation synchronized. we do not pass the cancellation token because either:
                    // a. the request had already failed(or canceled) and is about to be completed anyway
                    // b. the request had already completed successfully and there's no point in cancelling now 
                    await inflightData.Lock.WaitAsync();

                    // if the task has completed successfully
                    if (error is null)
                    {
                        // mark the pipe as completed
                        inflightData.ContentPipe.Writer.Complete();

                        inflightData.Completed = true;
                    }
                    else // an error had occured
                    {
                        // cast the NSError to an Exception
                        Exception e = error.AsException();

                        // if the operation has been canceled and it was due to the cancellation token, we assign 
                        // a new exception that contains the token
                        if (e is OperationCanceledException && inflightData.CancellationToken.IsCancellationRequested)
                            e = new OperationCanceledException(inflightData.CancellationToken);

                        // set the exception
                        inflightData.CompletionSource?.TrySetException(e);
                        inflightData.ResponseContent?.SetReadException(ExceptionDispatchInfo.Capture(e));
                        inflightData.Errored = true;
                    }

                    // report the progress of the task as completed if we haven't already
                    // this can only happen if we completed prematurely(with an error)
                    // or if we never reported progress in the first place(BytesExpectedToReceive <= 0)
                    if (error != null || task.BytesExpectedToReceive <= 0)
                    {
                        inflightData.Progress?.Report(1f);
                    }
                }
                finally
                {
                    // remove the data for the completed task
                    RemoveInflightDataFor(task);

                    // release the lock
                    inflightData.Lock.TryRelease();
                }
            }

        }

    }

}