﻿using System;
using CoreAnimation;

namespace RumorFinder.Animations
{

    internal partial class AnimatedExpandingTransition
    {

        /// <summary>
        ///     Used to add completion methods to CA animations
        /// </summary>
        private class AnimationDelegate : CAAnimationDelegate
        {

            #region Fields

            private readonly Action<bool> _completion;

            #endregion

            #region Constructors

            public AnimationDelegate(Action<bool> completion) => _completion = completion;

            #endregion

            /// <summary>
            ///     The animation has stopped.  Use the bool value to determine if this is a temporary pause, or the end of the
            ///     animation.
            /// </summary>
            /// <param name="anim">
            ///     <para>The CAAnimation object that has ended.</para>
            ///     <para tool="nullallowed">This parameter can be <see langword="null" />.</para>
            /// </param>
            /// <param name="finished">A flag indicating whether the animation has completed by reaching the end of its duration.</param>
            public override void AnimationStopped(CAAnimation anim, bool finished) => _completion(finished);

        }

    }

}