﻿using CoreGraphics;
using UIKit;

namespace RumorFinder.Animations
{
    public class AnimatedHorizontalTransition : UIViewControllerAnimatedTransitioning
    {
        #region Fields

        private readonly double _animationDuration;
        private readonly bool _directionIsLeftToRight;

        #endregion

        #region Constructors

        /// <summary>
        ///     Create a horizontal transition between two view controllers.
        /// </summary>
        /// <param name="animationDuration">The duration of animation.</param>
        /// <param name="directionIsLeftToRight">
        ///     a bool indicating the direction of the animation.
        ///     Pass true if the direction is left to right or false if the direction is right to left.
        /// </param>
        public AnimatedHorizontalTransition(double animationDuration, bool directionIsLeftToRight)
        {
            _animationDuration = animationDuration;
            _directionIsLeftToRight = directionIsLeftToRight;
        }

        #endregion

        /// <summary>Returns the duration, in seconds, of the transition animation.</summary>
        /// <param name="transitionContext">The context object containing information to use during the transition.</param>
        /// <returns>The duration, in seconds, of the custom transition animation.</returns>
        public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext) =>
            _animationDuration;

        /// <summary>Performs a custom UIViewController transition animation.</summary>
        /// <param name="transitionContext">The context object containing information to use during the transition.</param>
        public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
        {
            // get the needed views for the animations
            UIView containerView = transitionContext.ContainerView;

            UIView fromView = transitionContext.GetViewFor(UITransitionContext.FromViewKey);
            UIView toView = transitionContext.GetViewFor(UITransitionContext.ToViewKey);

            CGRect fromViewFrame = fromView.Frame;
            CGRect toViewFrame = toView.Frame;

            double enteringFrameX =
                fromViewFrame.X + (_directionIsLeftToRight ? -toViewFrame.Width : toViewFrame.Width);
            double exitingFrameX =
                fromViewFrame.X + (_directionIsLeftToRight ? fromViewFrame.Width : -fromViewFrame.Width);

            CGRect enteringFrame = new CGRect(enteringFrameX, fromViewFrame.Y, toViewFrame.Width, toViewFrame.Height);
            CGRect exitingFrame = new CGRect(exitingFrameX, fromViewFrame.Y, fromViewFrame.Width, fromViewFrame.Height);
            CGRect finalFrame = new CGRect(fromViewFrame.X, fromViewFrame.Y, toViewFrame.Width, toViewFrame.Height);

            toView.Frame = enteringFrame;
            containerView.Add(toView);

            UIViewPropertyAnimator.CreateRunningPropertyAnimator(_animationDuration,
                                                                 0,
                                                                 UIViewAnimationOptions.CurveEaseInOut,
                                                                 () =>
                                                                 {
                                                                     fromView.Frame = exitingFrame;
                                                                     toView.Frame = finalFrame;
                                                                 }, 
                                                                 position => 
                                                                     transitionContext.CompleteTransition(
                                                                         position == UIViewAnimatingPosition.End)
                                                                 );
        }
    }
}