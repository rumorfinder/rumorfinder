﻿using CoreGraphics;
using UIKit;

namespace RumorFinder.Extensions.UIKit
{

    internal static class ButtonExtensions
    {

        /// <summary>
        ///     sets an UiImage as the image of a UIButton and adjusts the frame of the button accordingly
        /// </summary>
        /// <param name="button"></param>
        /// <param name="image"></param>
        public static void SetImage(this UIButton button, UIImage image)
        {
            button.SetImage(image, UIControlState.Normal);
            button.Frame = new CGRect(button.Frame.X, button.Frame.Y, image.Size.Width, image.Size.Height);
        }

    }

}