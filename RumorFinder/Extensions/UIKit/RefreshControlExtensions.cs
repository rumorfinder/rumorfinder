﻿using CoreAnimation;
using UIKit;

namespace RumorFinder.Extensions.UIKit
{
    public static class RefreshControlExtensions
    {
        /// <summary>
        ///     Animates the disappearing of the refresh control's spinner icon.
        /// </summary>
        public static void EndRefreshingAnimated(this UIRefreshControl refreshControl)
        {
            CATransaction.Begin();
            refreshControl.EndRefreshing();
            CATransaction.Commit();
        }
    }
}