﻿using UIKit;

namespace RumorFinder.Extensions.UIKit
{

    internal static class TextFieldExtensions
    {

        /// <summary>
        ///     sets the tint color of the clear button that belongs to the text field
        /// </summary>
        /// <param name="textField"></param>
        /// <param name="tintColor"></param>
        public static void SetClearButtonTintColor(this UITextField textField, UIColor tintColor)
        {
            foreach (UIView subview in textField.Subviews)
            {
                if (subview is UIButton clearButton) clearButton.SetTitleColor(tintColor, UIControlState.Normal);
            }
        }

    }

}