﻿using System;
using Foundation;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Extensions.RumorFinder.Foundation
{

    internal static class RumorExtensions
    {

        /// <summary>
        ///     creates an attributed string from the content and updates of the supplied rumor object
        /// </summary>
        /// <param name="rumor"></param>
        /// <param name="contentAttributes">the attributes to be applied to the content part of the string</param>
        /// <param name="updatesAttributes">the attributes to be applied to the updates part of the string</param>
        /// <returns></returns>
        public static NSAttributedString ToAttributedString(
            this Rumor rumor,
            UIStringAttributes contentAttributes,
            UIStringAttributes updatesAttributes)
        {
            //append the content and the updates into one string and create an attributed string from it
            string entireText = $"{rumor.Content}\n\n{rumor.Updates}";
            var entireAttributedText =
                new NSMutableAttributedString(new NSAttributedString(entireText, contentAttributes));

            //prepare the information needed to bold the updates' text
            int updatesOffset = entireText.IndexOf(rumor.Updates, StringComparison.CurrentCulture);
            var updatesRange = new NSRange(updatesOffset, entireText.Length - updatesOffset);

            //bold the updates' text
            entireAttributedText.AddAttributes(updatesAttributes, updatesRange);

            return entireAttributedText;
        }

    }

}