﻿namespace RumorFinder.Assets
{

    internal enum Storyboards
    {

        Main = 0,
        HomeView = 1,
        FollowedProductsView = 2,
        ProductView = 3

    }

}