﻿using System.Collections.Generic;
using RumorFinder.Foundation;

namespace RumorFinder.Util
{

    public partial class User
    {

        /// <summary>
        ///     represents a comparer between two followed products
        /// </summary>
        private class FollowedProductsComparer : IComparer<Product>
        {

            /// <summary>
            ///     Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the
            ///     other.
            /// </summary>
            /// <param name="x">The first object to compare.</param>
            /// <param name="y">The second object to compare.</param>
            /// <returns>
            ///     A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in
            ///     the following table.Value Meaning Less than zero
            ///     <paramref name="x" /> is less than <paramref name="y" />.Zero
            ///     <paramref name="x" /> equals <paramref name="y" />.Greater than zero
            ///     <paramref name="x" /> is greater than <paramref name="y" />.
            /// </returns>
            public int Compare(Product x, Product y)
            {
                // we want the products with updates to appear before the products
                // who don't have updates in a list that is in ascending order
                if (y.Rumors.HasUpdates ^ x.Rumors.HasUpdates)
                {
                    return x.Rumors.HasUpdates ? -1 : 1;
                }

                return x.CompareTo(y);
            }

        }

    }

}