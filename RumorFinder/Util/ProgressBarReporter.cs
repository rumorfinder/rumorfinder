﻿using System;
using UIKit;

namespace RumorFinder.Util
{

    internal class ProgressViewReporter : IProgress<float>
    {

        #region Fields

        private readonly UIProgressView _progressView;

        #endregion

        #region Constructors

        public ProgressViewReporter(UIProgressView progressView) => _progressView = progressView;

        #endregion

        /// <inheritdoc />
        /// <summary>Reports a progress update.</summary>
        /// <param name="value">The value of the updated progress.</param>
        void IProgress<float>.Report(float value)
        {
            _progressView.InvokeOnMainThread(() => _progressView.SetProgress(value, true));
        }

    }

}