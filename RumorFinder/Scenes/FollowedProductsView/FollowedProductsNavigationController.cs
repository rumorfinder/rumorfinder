﻿using System;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{

    public partial class FollowedProductsNavigationController : UINavigationController
    {

        #region Constructors

        public FollowedProductsNavigationController(IntPtr handle) : base(handle)
        { }

        #endregion

    }

}