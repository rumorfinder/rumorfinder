﻿using System;
using CoreAnimation;
using CoreGraphics;
using RumorFinder.Assets;
using RumorFinder.Foundation;
using UIKit;

namespace RumorFinder.Scenes.FollowedProductsView
{
    public partial class FollowedProductTableCell : UITableViewCell
    {
        #region Static Fields and Constants

        private const double AnimationDuration = 2.5;

        private static readonly nfloat IconSpacing = 15;

        #endregion

        #region Fields

        private bool _initializedIcon;

        private UIImageView _notificationIcon;

        #endregion

        #region Properties

        public Product DisplayedProduct { get; private set; }

        #endregion

        #region Constructors

        public FollowedProductTableCell(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     Sets the new displayed product and updates the view's attributes.
        /// </summary>
        /// <param name="newDisplayedProduct">The new displayed product for this cell.</param>
        public void SetDisplayedProduct(Product newDisplayedProduct)
        {
            if (!_initializedIcon)
            {
                SetupNotificationIcon();
                _initializedIcon = true;
            }

            // remove the event listener from the previous product
            if (DisplayedProduct != null) DisplayedProduct.RumorsChanged -= OnRumorsChange;

            // update the attributes
            ProductName.Text = newDisplayedProduct.Name;

            DisplayedProduct = newDisplayedProduct;

            // add the event listener to make sure the icon stays updated
            DisplayedProduct.RumorsChanged += OnRumorsChange;

            // update the notification icon
            UpdateNotificationIcon();
        }

        /// <summary>
        ///     Event listener for the displayed product's rumors changed event.
        /// </summary>
        /// <param name="sender">the sender of the event.</param>
        /// <param name="e">the arguments of the event.</param>
        private void OnRumorsChange(object sender, EventArgs e) => InvokeOnMainThread(UpdateNotificationIcon);

        /// <summary>
        ///     Updates the notification icon according to the update status of the currently displayed product
        /// </summary>
        private void UpdateNotificationIcon()
        {
            // get the new alpha value
            nfloat iconAlpha = DisplayedProduct.Rumors.HasUpdates ? 100 : 0;

            // animate the change
            UIViewPropertyAnimator.CreateRunningPropertyAnimator(FollowedProductTableCell.AnimationDuration,
                                                                 0,
                                                                 UIViewAnimationOptions.CurveEaseInOut,
                                                                 () => _notificationIcon.Alpha = iconAlpha,
                                                                 null);
        }

        /// <summary>
        ///     Creates and adds the notification icon to the cell
        /// </summary>
        private void SetupNotificationIcon()
        {
            UIImage icon = Icons.Notification;
            CGRect superFrame = Content.Frame;

            // create the image view
            double x = superFrame.Width - icon.Size.Width - FollowedProductTableCell.IconSpacing;
            double y = ProductName.Center.Y - icon.Size.Height / 2;
            UIImageView iconView = new UIImageView(new CGRect(x, y, icon.Size.Width, icon.Size.Height))
            {
                Image = icon,
                Alpha = 0 // the icon starts out hidden
            };

            // add it to the table cell
            Content.Add(iconView);

            // save the image view so it can be referenced later
            _notificationIcon = iconView;
        }
    }
}