﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace RumorFinder.Scenes.FollowedProductsView
{
    [Register ("FollowedProductTableCell")]
    partial class FollowedProductTableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Content { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ProductName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Content != null) {
                Content.Dispose ();
                Content = null;
            }

            if (ProductName != null) {
                ProductName.Dispose ();
                ProductName = null;
            }
        }
    }
}