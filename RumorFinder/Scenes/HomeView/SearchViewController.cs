﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Foundation;
using RumorFinder.Assets;
using RumorFinder.Extensions.Assets;
using RumorFinder.Extensions.Foundation;
using RumorFinder.Extensions.System;
using RumorFinder.Foundation;
using RumorFinder.Scenes.Shared.ProductView;
using RumorFinder.Util;
using UIKit;

namespace RumorFinder.Scenes.HomeView
{

    public partial class SearchViewController : UIViewController, IUISearchBarDelegate
    {

        #region Static Fields and Constants

        private const string SearchBarPrompt = "Find rumors about...";

        internal static readonly Suggestion[] SupportedSuggestions = {Suggestion.RecentSearch, Suggestion.Recommendation};

        private const string ErrorAlertTitle = "Cannot Update";
        private const string ErrorAlertMessage =
            "Could not perform the search, please check your internet connection and try again later.";

        #endregion

        #region Fields

        private UISearchController _searchController;

        private CancellationTokenSource _currentRefresh;

        #endregion

        #region Properties

        private IReadOnlyList<Product> RecentSearches => this.GetCurrentUser().RecentSearches;

        #endregion

        #region Constructors

        public SearchViewController(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     returns the correct handler for a header with the given suggestion
        /// </summary>
        /// <param name="suggestion"></param>
        /// <returns></returns>
        private EventHandler GetHeaderButtonHandler(Suggestion suggestion)
        {
            return new Dictionary<Suggestion, EventHandler>
            {
                [Suggestion.RecentSearch] = ClearAllRecentSearches,
                [Suggestion.Recommendation] = RefreshRecommendations
            }[suggestion];
        }

        /// <summary>
        ///     adds a new recent search and makes sure the size doesn't exceeds the maximum
        /// </summary>
        /// <param name="recentSearch">
        ///     the recent search made by the user. if the user already searched this item, the old item is
        ///     assigned to the variable and the old cell is just moved to the top
        /// </param>
        private void AddRecentSearch(ref Product recentSearch)
        {
            // add the recent search
            this.GetCurrentUser().AddRecentSearch(ref recentSearch, out int oldIndex, out int newIndex,
                                                  out bool exceededMaximum);

            // update the ui
            int section = Array.IndexOf(SearchViewController.SupportedSuggestions, Suggestion.RecentSearch);
            // if the recent search was already in the list
            if (oldIndex >= 0)
            {
                var oldIndexPath = NSIndexPath.FromRowSection(oldIndex, section);
                var newIndexPath = NSIndexPath.FromRowSection(newIndex, section);
                SuggestionTableView.MoveRow(oldIndexPath, newIndexPath);
            }
            else // animate the addition of the new recent search
            {
                var newIndexPath = NSIndexPath.FromRowSection(newIndex, section);
                SuggestionTableView.InsertRows(newIndexPath.AsOneObjectArray(), UITableViewRowAnimation.Top);
            }

            // if we removed the bottom entry of the list, we reload the data
            if (exceededMaximum)
                SuggestionTableView.ReloadData();
        }

        /// <summary>
        ///     clears all the recent searches
        /// </summary>
        private void ClearAllRecentSearches(object sender, EventArgs e)
        {
            NSIndexPath[] indexes = RecentSearches.GetIndexPaths();
            this.GetCurrentUser().ClearSearchHistory();

            //clear the entire table
            SuggestionTableView.DeleteRows(indexes, UITableViewRowAnimation.Fade);
        }

        private async void RefreshRecommendations(object sender, EventArgs e)
        {
            _currentRefresh = new CancellationTokenSource();
            CancellationToken token = _currentRefresh.Token;

            try
            {
                // disable the button to avoid duplicate requests
                ((UIButton) sender).Enabled = false;

                OperationStatus status = await this.GetCurrentUser().RefreshRecommendationsAsync(token)
                                                   .ConfigureAwait(false);

                Action completion = delegate {};
                switch (status)
                {
                    // nothing to do if the refresh was unsuccessful
                    case OperationStatus.Unsuccessful:
                        return;
                    // in the event of failure we present to the user
                    // a notification 
                    case OperationStatus.Failed:
                        completion = HandleFailure;
                        break;
                    // in the event of success we insert the new recommendations
                    case OperationStatus.Successful:
                        completion = ShowNewRecommendations;
                        break;
                }

                // execute our completion function
                InvokeOnMainThread(completion);
            }
            catch (OperationCanceledException)
            { }
            finally
            {
                // reset the cancellation token source and
                // re enable the refresh button
                InvokeOnMainThread(() =>
                {
                    ((UIButton) sender).Enabled = true;
                });
                _currentRefresh = null;
            }

            // cleanup method to notify the user the refresh failed
            void HandleFailure()
            {
                // configure the alert
                var alert = UIAlertController.Create(SearchViewController.ErrorAlertTitle,
                                                     SearchViewController.ErrorAlertMessage,
                                                     UIAlertControllerStyle.Alert);
                alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                // present the alert to the user
                PresentViewController(alert, true, null);
            }

            // helper method that presents to the user the new recommendations
            void ShowNewRecommendations()
            {
                // refresh the recommendation section to display the new recommendations
                nint section =
                         Array.IndexOf(SearchViewController.SupportedSuggestions, Suggestion.Recommendation);
                SuggestionTableView.ReloadSections(NSIndexSet.FromIndex(section), UITableViewRowAnimation.Fade);
            }

        }

        /// <summary>
        ///     displays the product in a product view controller
        /// </summary>
        /// <param name="searchedProduct"></param>
        private void DisplayProduct(Product searchedProduct)
        {
            var productView = Storyboards.ProductView.InstantiateViewController<ProductViewController>();
            productView.SetDisplayedProduct(searchedProduct);
            ShowViewController(productView, this);
        }

        /// <summary>
        ///     sets up and configures the view's search bar
        /// </summary>
        private void SetUpSearchBar()
        {
            //configure the search bar
            var searchController = new UISearchController(searchResultsController: null)
            {
                HidesNavigationBarDuringPresentation = false
            };
            searchController.SearchBar.KeyboardAppearance = UIKeyboardAppearance.Dark;
            searchController.SearchBar.SearchBarStyle = UISearchBarStyle.Minimal;
            searchController.SearchBar.BarStyle = UIBarStyle.Black;
            searchController.SearchBar.BarTintColor = Colors.MainBackgroundColor;
            searchController.SearchBar.Placeholder = SearchViewController.SearchBarPrompt;
            searchController.SearchBar.Delegate = this;

            //configure the navigation bar
            NavigationItem.Title = string.Empty;
            NavigationItem.LargeTitleDisplayMode = UINavigationItemLargeTitleDisplayMode.Never;
            NavigationItem.TitleView = searchController.SearchBar;

            // save the search controller so it can be referenced later
            _searchController = searchController;
        }

        #region IUISearchBarDelegateMethods

        /// <summary>
        ///     methods that is called when the user presses the search key
        /// </summary>
        /// <param name="searchBar"></param>
        /// <returns></returns>
        [Export("searchBarSearchButtonClicked:")]
        public void SearchButtonClicked(UISearchBar searchBar)
        {
            if (string.IsNullOrEmpty(searchBar.Text))
                return;

            var search = new Product(searchBar.Text.Trim());

            // if we already follow the product we take that object instead
            if (this.GetCurrentUser().Follows(search))
            {
                var followedProducts = this.GetCurrentUser().FollowedProducts;
                search = followedProducts.First(p => p.Equals(search));
            }

            AddRecentSearch(ref search);

            //display the user's search
            DisplayProduct(search);
        }

        #endregion

        #region View Controller Overrides

        /// <summary>Called after the controller’s <see cref="P:UIKit.UIViewController.View" /> is loaded into memory.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after <c>this</c> <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> and its entire view hierarchy have been loaded into memory. This
        ///         method is called whether the <see cref="T:UIKit.UIView" /> was loaded from a .xib file or programmatically.
        ///     </para>
        /// </remarks>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //create and assign the back bar button
            var backButton = new UIBarButtonItem {Image = Icons.BackButton};
            backButton.Clicked +=
                (sender, e) => NavigationController?.DismissViewController(true, null);
            NavigationItem.LeftBarButtonItem = backButton;

            SetUpSearchBar();

            //set up the suggestions table view
            SuggestionTableView.Source = new SuggestionsTableSource
                {Superview = this};
            SuggestionTableView.ReloadData();

            // enable large titles
            if (NavigationController != null) NavigationController.NavigationBar.PrefersLargeTitles = true;
        }

        /// <param name="animated">
        ///     <para>If the appearance will be animated.</para>
        /// </param>
        /// <summary>
        ///     Called prior to the <see cref="P:UIKit.UIViewController.View" /> being added to the view hierarchy.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is called prior to the <see cref="T:UIKit.UIView" /> that is this
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> property being added to
        ///         the display <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewWillAppear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </remarks>
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // refresh the suggestions
            SuggestionTableView.ReloadData();

            // we hide and then show the navigation bar to "refresh" it and to solve visual bugs that occur
            NavigationController?.SetNavigationBarHidden(true, false);
            NavigationController?.SetNavigationBarHidden(false, false);
        }

        /// <param name="animated">
        ///     <para>If the appearance will be animated.</para>
        /// </param>
        /// <summary>
        ///     <para>
        ///         This method is called prior to the removal of the <see cref="T:UIKit.UIView" />that is this
        ///         <see cref="T:UIKit.UIViewController" />’s <see cref="P:UIKit.UIViewController.View" /> from the display
        ///         <see cref="T:UIKit.UIView" /> hierarchy.
        ///     </para>
        ///     <para>
        ///         Application developers may override this method to configure animations, resign first responder status (see
        ///         <see cref="M:UIKit.UIResponder.ResignFirstResponder" />), or perform other tasks.
        ///     </para>
        ///     <para>
        ///         Application developers who override this method must call <c>base.ViewWillDisappear()</c> in their overridden
        ///         method.
        ///     </para>
        /// </summary>
        /// <remarks></remarks>
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            // dismiss the search bar
            _searchController.Active = false;

            // cancel any ongoing refresh
            _currentRefresh?.Cancel();
        }

        #endregion

    }

}