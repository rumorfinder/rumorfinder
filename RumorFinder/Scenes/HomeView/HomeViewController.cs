﻿using System;
using Foundation;
using RumorFinder.Animations;
using RumorFinder.Extensions.UIKit;
using UIKit;

namespace RumorFinder.Scenes.HomeView
{

    public partial class HomeViewController : UIViewController, IUIViewControllerTransitioningDelegate
    {

        #region Static Fields and Constants

        private const double TransitionDuration = 0.2;

        private const int DistanceBetweenButtonAndTitle = 10;

        private const string SearchViewSegueIdentifier = "SearchViewSegue";

        #endregion

        #region Constructors

        public HomeViewController(IntPtr handle) : base(handle)
        { }

        #endregion

        /// <summary>
        ///     sets up the constraints for the View's subviews
        /// </summary>
        private void SetupConstraints()
        {
            SearchButton.RemoveAllConstraints();
            SearchButton.TranslatesAutoresizingMaskIntoConstraints = false;

            MainTitle.RemoveAllConstraints();
            MainTitle.TranslatesAutoresizingMaskIntoConstraints = false;

            SearchButton.CenterXAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.CenterXAnchor).Active = true;
            SearchButton.CenterYAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.CenterYAnchor).Active = true;
            SearchButton.WidthAnchor.ConstraintEqualTo(SearchButton.Bounds.Width).Active = true;
            SearchButton.HeightAnchor.ConstraintEqualTo(SearchButton.Bounds.Height).Active = true;
            SearchButton
                .TopAnchor.ConstraintEqualTo(MainTitle.BottomAnchor, HomeViewController.DistanceBetweenButtonAndTitle)
                .Active = true;
            MainTitle.CenterXAnchor.ConstraintEqualTo(SearchButton.CenterXAnchor).Active = true;
        }

        #region View Controller Overrides

        /// <summary>Called after the controller’s <see cref="P:UIKit.UIViewController.View" /> is loaded into memory.</summary>
        /// <remarks>
        ///     <para>
        ///         This method is called after <c>this</c> <see cref="T:UIKit.UIViewController" />'s
        ///         <see cref="P:UIKit.UIViewController.View" /> and its entire view hierarchy have been loaded into memory. This
        ///         method is called whether the <see cref="T:UIKit.UIView" /> was loaded from a .xib file or programmatically.
        ///     </para>
        /// </remarks>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.AutosizesSubviews = false;

            // set the search button's border attributes
            SearchButton.Layer.BorderWidth = (nfloat) 0.2;
            SearchButton.Layer.BorderColor = UIColor.LightGray.CGColor;

            // setting up constraints in code due to a bug that caused the views to be enlarged
            SetupConstraints();
        }

        /// <summary>
        ///     Notifies the view controller that a segue is about to be performed.
        /// </summary>
        /// <param name="segue">The segue object containing information about the view controllers involved in the segue.</param>
        /// <param name="sender">
        ///     The object that initiated the segue. You might use this parameter to perform different actions
        ///     based on which control (or other object) initiated the segue.
        /// </param>
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            if (segue.Identifier != HomeViewController.SearchViewSegueIdentifier)
                return;

            // set up custom transitions
            UIViewController destinationViewController = segue.DestinationViewController;
            destinationViewController.TransitioningDelegate = this;
            destinationViewController.ModalPresentationStyle = UIModalPresentationStyle.Custom;
        }

        #endregion

        #region Transitioning Delegate Methods

        [Export("animationControllerForPresentedController:presentingController:sourceController:")]
        public IUIViewControllerAnimatedTransitioning GetAnimationControllerForPresentedController(
            UIViewController presented,
            UIViewController presenting,
            UIViewController source) => new AnimatedExpandingTransition(SearchButton, SearchButton.Frame,
                                                                        HomeViewController.TransitionDuration,
                                                                        true);

        [Export("animationControllerForDismissedController:")]
        public IUIViewControllerAnimatedTransitioning GetAnimationControllerForDismissedController(
            UIViewController dismissed) =>
            new AnimatedExpandingTransition(SearchButton, SearchButton.Frame,
                                            HomeViewController.TransitionDuration,
                                            false);

        #endregion

    }

}