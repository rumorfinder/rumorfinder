﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace RumorFinder.Scenes.Shared.ProductView
{
    [Register ("ProductViewController")]
    partial class ProductViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView DisplayedRumors { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView ProgressBar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DisplayedRumors != null) {
                DisplayedRumors.Dispose ();
                DisplayedRumors = null;
            }

            if (ProgressBar != null) {
                ProgressBar.Dispose ();
                ProgressBar = null;
            }
        }
    }
}