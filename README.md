# RumorFinder
This repository contains the code for the RumorFinder app which allows users to search for online rumors about upcoming technological products(like phones, laptops etc').

### Currently this repository contains three projects:

## 1. The RumorFinder project: 
This is a Xamarin.iOS prject that contains the code for the app.
## 2. The RumorFinder.Foundation project:
This is a .NET standard class library that contains the business logic of the app (mainly the code that search for rumors about products).
## 3. The RumorFinder.NativeHttp project:
This is a multi target class library that wraps the NSUrlSession library so that it can be used with the .NET HttpClient class and report the progress of ongoing HTTP requests (which currently isn't available with the HttpClient class.


### [Current state of the app.](https://streamable.com/85fvm) 

### [Documentation for the project.](https://galschwietzer.gitlab.io/rumorfinder)

### [The RumorFinder Server project.](https://gitlab.com/galschwietzer/rumorfinder.server)